#[derive(Copy, Clone, Debug)]
enum Hand {
    Rock,
    Paper,
    Scissors,
}
use Hand::*;

type Strategy = Vec<(Hand, Hand)>;

fn hand_from_ch(ch: &str) -> Hand {
    match ch {
        "A" | "X" => Rock,
        "B" | "Y" => Paper,
        "C" | "Z" => Scissors,
        _ => panic!("Invalid character {}", ch),
    }
}

fn read_line(text: &str) -> (Hand, Hand) {
    let hands: Vec<Hand> = text.split(" ").map(hand_from_ch).collect();
    assert_eq!(hands.len(), 2);
    (hands[0], hands[1])
}

fn read_strategy(text: &str) -> Strategy {
    text.split("\n")
        .filter(|line| line.len() > 0)
        .map(read_line)
        .collect()
}

fn score(fight: &(Hand, Hand)) -> i32 {
    let result = (fight.1 as i32 - fight.0 as i32 + 3) % 3;
    let fight_score = match result {
        0 => 3,
        1 => 6,
        2 => 0,
        _ => panic!("Invalid result {}", result),
    };
    let hand_score = fight.1 as i32 + 1;

    hand_score + fight_score
}

fn convert_to_result(hand: Hand, result: Hand) -> Hand {
    let i_hand = match result {
        Scissors => ((hand as i32) + 1) % 3,
        Paper => hand as i32,
        Rock => ((hand as i32) + 2) % 3,
    };

    match i_hand {
        0 => Rock,
        1 => Paper,
        2 => Scissors,
        _ => panic!(
            "Invalid i_hand {} from hand {:?} and result {:?}",
            i_hand, hand, result
        ),
    }
}

fn transform_to_new_strat(hands: &(Hand, Hand)) -> (Hand, Hand) {
    // For part 2 the second hand needs to be transformed
    // so we match the new rules: Rock -> Lose, Paper -> Draw, Scissors -> Win

    let new_hand = convert_to_result(hands.0, hands.1);

    (hands.0, new_hand)
}

fn main() {
    let input = std::fs::read_to_string("input.txt").expect("Couldnt read file");

    let strategy = read_strategy(&input);

    println!("Score part 1: {}", strategy.iter().map(score).sum::<i32>());

    println!(
        "Score part 2: {}",
        strategy
            .iter()
            .map(transform_to_new_strat)
            .map(|h| score(&h))
            .sum::<i32>()
    );

    println!("Merry Christmas!");
}
