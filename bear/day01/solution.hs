import Data.List

getElves :: [String] -> [[Int]]
getElves strLines = go strLines [] []
  where
    go [] tempAcc acc = (tempAcc:acc)
    go ("":ls) tempAcc acc = go ls [] (tempAcc:acc)
    go (val:ls) tempAcc acc = go ls (read val:tempAcc) acc



topThreeSnacks :: [[Int]] -> [Int]
topThreeSnacks cals = take 3 $ sortOn negate $ map sum cals


main = do
  lines <- lines <$> readFile "input.txt"
  -- putStrLn $ show lines
  let elfCalories = getElves lines
  -- putStrLn $ show elfCalories
  putStrLn $ "Max cal: " ++ (show $ maximum $ map sum elfCalories)
  putStrLn $ "Top 3: " ++ (show $ topThreeSnacks elfCalories)
  putStrLn $ "Top 3 sum: " ++ (show $ sum $ topThreeSnacks elfCalories)
  putStrLn "Christmas is here!"
