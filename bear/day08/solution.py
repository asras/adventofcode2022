from collections import defaultdict

def read_trees(fname):
    with open(fname, "r") as f:
        tree_rows = f.read().split("\n")

    tree_rows = [[int(c) for c in r] for r in tree_rows if len(r) > 0]

    return tree_rows


def scan_rows(tree_rows, tree_marks, scan_forward):
    n_rows = len(tree_rows)
    n_cols = len(tree_rows[0])

    col_range = range(n_cols) if scan_forward else range(n_cols - 1, -1, -1)
    for row in range(n_rows):
        max_height = -1
        for col in col_range:
            height = tree_rows[row][col]
            if height > max_height:
                max_height = height
                tree_marks[row][col].append(1)
            else:
                tree_marks[row][col].append(0)


def scan_cols(tree_rows, tree_marks, scan_forward):
    n_rows = len(tree_rows)
    n_cols = len(tree_rows[0])

    row_range = range(n_rows) if scan_forward else range(n_rows - 1, -1, -1)
    for col in range(n_cols):
        max_height = -1
        for row in row_range:
            height = tree_rows[row][col]
            if height > max_height:
                max_height = height
                tree_marks[row][col].append(1)
            else:
                tree_marks[row][col].append(0)



def mark_trees(tree_rows):
    # Mark each tree with four booleans
    # indicating whether they are visible in the four directions
    n_rows = len(tree_rows)
    n_cols = len(tree_rows[0])
    tree_marks = []
    for i in range(n_rows):
        row_list = []
        for j in range(n_cols):
            row_list.append([])
        tree_marks.append(row_list)

    # Scan rows left to right
    scan_rows(tree_rows, tree_marks, scan_forward=True)
    # Scan rows right to left
    scan_rows(tree_rows, tree_marks, scan_forward=False)

    # Scan rows top to bottom
    scan_cols(tree_rows, tree_marks, scan_forward=True)

    # Scan rows bottom to top
    scan_cols(tree_rows, tree_marks, scan_forward=False)

    return tree_marks


def calculate_visible_trees(tree_marks):
    return sum(map(sum, map(lambda row: map(max, row), tree_marks)))


def part1(debug=False):
    fname = "test.txt" if debug else "input.txt"
    rows = read_trees(fname)
    marks = mark_trees(rows)
    visible = calculate_visible_trees(marks)

    if debug:
        for row in marks:
            for col in row:
                if max(col) > 0:
                    print("1", end="")
                else:
                    print("0", end="")
                print("")
            
    print(f"{visible=}")


part1()


def score_trees(tree_rows):
    n_rows = len(tree_rows)
    n_cols = len(tree_rows[0])

    scores = []
    for row in range(n_rows):
        # Make score row
        score_row = []
        for col in range(n_cols):
            tree_score = calculate_score(row, col, tree_rows)
            score_row.append(tree_score)

        scores.append(score_row)

    return scores


def calculate_score(row, col, tree_rows):
    dist_up = view_dist(row, col, tree_rows, "up")
    dist_right = view_dist(row, col, tree_rows, "right")
    dist_down = view_dist(row, col, tree_rows, "down")
    dist_left = view_dist(row, col, tree_rows, "left")
    
    return dist_up * dist_right * dist_down * dist_left


def view_dist(row, col, tree_rows, scan_dir):
    n_rows = len(tree_rows)
    n_cols = len(tree_rows[0])

    match scan_dir:
        case "up":
            row_range = range(row - 1, -1, -1)
            col_range = range(col, col + 1)
        case "right":
            row_range = range(row, row + 1)
            col_range = range(col + 1, n_cols)
        case "down":
            row_range = range(row + 1, n_rows)
            col_range = range(col, col + 1)
        case "left":
            row_range = range(row, row + 1)
            col_range = range(col - 1, -1, -1)
        case x:
            raise Exception(f"Unknown dir {scan_dir}")
            

    starting_height = tree_rows[row][col]
    dist = 0
    for current_row in row_range:
        for current_col in col_range:
            dist += 1
            height = tree_rows[current_row][current_col]
            if height >= starting_height:
                return dist

    return dist

def part2(debug=False):
    fname = "test.txt" if debug else "input.txt"
    rows = read_trees(fname)
    scenic_scores = score_trees(rows)

    max_score = max(map(max, scenic_scores))

    print(f"{max_score=}")


part2()
    
