from __future__ import annotations

from dataclasses import dataclass
from typing import List, Optional



@dataclass
class Node:
    idx: int
    row: int
    col: int
    neighbors: List[Node]
    val: str
    path_length: Optional[int]

    def __hash__(self):
        return self.row * 100_000 + self.col


def height_from_char(char):
    if ord(char) >= ord("a") and ord(char) <= ord("z"):
        return ord(char) - ord("a")
    elif char == "S":
        return 0
    elif char == "E":
        return ord("z") - ord("a")
    else:
        raise ValueError(f"Invalid char: {char}")



def neighbors(r, c, width, height):
    if r - 1 >= 0:
        yield (r - 1), c
    if r + 1 < height:
        yield (r + 1), c
    if c - 1 >= 0:
        yield r, c - 1
    if c + 1 < width:
        yield r, c + 1


def read_grid(fname):
    with open(fname, "r") as f:
        contents = f.read()

    rows = [r for r in contents.split("\n") if len(r) > 0]
    n_rows = len(rows)
    n_cols = len(rows[0])
    print(f"{n_rows=}, {n_cols=}")

    def idx(r, c):
        return r * n_cols + c

    nodes = [Node(idx(r, c), r, c, [], rows[r][c], None)
             for r in range(n_rows)
             for c in range(n_cols)]

    start_loc = None
    goal_loc = None
    a_idxs = []
    
    for r in range(n_rows):
        for c in range(n_cols):
            if rows[r][c] == "a":
                a_idxs.append(idx(r, c))
            if rows[r][c] == "S":
                start_loc = (r, c)
            elif rows[r][c] == "E":
                goal_loc = (r, c)
            
            my_height = height_from_char(rows[r][c])
            my_idx = idx(r, c)
            for n_r, n_c in neighbors(r, c, n_cols, n_rows):
                n_height = height_from_char(rows[n_r][n_c])
                if n_height - my_height <= 1:
                    # Make a path FROM myself TO neighbor
                    # NOT the other way
                    n_idx = idx(n_r, n_c)
                    me = nodes[my_idx]
                    neigh = nodes[n_idx]
                    me.neighbors.append(neigh)



    return nodes, idx(*start_loc), idx(*goal_loc)


def search(nodes, start, goal, verbose=True):
    current_node = nodes[start]
    current_node.path_length = 0

    # Stack keeping track of who to visit
    next_visit = []

   
    for neigh in current_node.neighbors:
        # Also keep track of path we took to get to this node
        next_visit.append((neigh, [current_node]))

    cnt = 0
    while len(next_visit) > 0:
        cnt += 1
        if verbose:
            print(f"Visited {cnt} nodes", end="\r")
        current_node, current_path = next_visit.pop(0)

        if current_node.path_length is not None and current_node.path_length <= len(current_path):
            continue
        else:
            current_node.path_length = len(current_path)

        for neigh in current_node.neighbors:
            # Dont visit this neighbor if it is closer to the start
            if neigh.path_length is not None and neigh.path_length <= current_node.path_length + 1:
                continue

            
            this_path = current_path.copy()
            this_path.append(current_node)
            next_visit.append((neigh, this_path))

    if verbose:
        print(f"Visited {cnt} nodes during search")

    # A fun little debug print
    # for row in range(41):
    #     for col in range(171):
    #         node = nodes[(row * 171 + col) % len(nodes)]
    #         val = "." if node.path_length is not None else node.val
    #         print(val, end="")
    #         # val = -1 if node.path_length is None else node.path_length
    #         # print("|", f"{val:4d}", "|", end="")
    #     print()

    return nodes[goal].path_length
    

def part1(debug=False):
    fname = "test.txt" if debug else "input.txt"
    nodes, start, goal = read_grid(fname)

    print(f"Found {len(nodes)} nodes")

    min_path = search(nodes, start, goal)
    
    print(f"Minimum distance to top of the hill: {min_path}")


import sys
if len(sys.argv) > 1:
    debug = sys.argv[1] == "1"
else:
    debug = False

part1(debug)


def part2(debug=False):
    fname = "test.txt" if debug else "input.txt"
    nodes, start, goal = read_grid(fname)
    starts = [node.idx for node in nodes if node.val == "a"]
    print(f"Found {len(nodes)} nodes")

    min_dist = None
    # Just looping over all as is not the most efficient way since
    # we will keep visiting isolated islands
    bad_as = set()
    n_searches = 0
    for start in starts:
        if start in bad_as:
            continue
        
        md = search(nodes, start, goal, verbose=False)
        n_searches += 1
        print(f"Finished search {n_searches}", end="\r")
        # There are islands of as that we cannot leave so md will be None
        if md is None:
            # Find bad as
            for node in nodes:
                if node.path_length is not None:
                    bad_as.add(node.idx)
            continue

        for node in nodes:
            node.path_length = None


        if min_dist is None or md < min_dist:
            min_dist = md

    print(f"Searched {n_searches} times   ")
    print(f"Starting from the bottom going to the top: {min_dist}")
    

print("--------------------")
part2(debug)    


print("MERRY CHRISTMAS!")
