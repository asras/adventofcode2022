use std::collections::HashMap;

#[derive(Debug, Copy, Clone, PartialEq)]
enum Square {
    Empty,
    Open,
    Wall,
}

#[derive(Debug, Copy, Clone)]
enum Direction {
    Up,
    Down,
    Right,
    Left,
}

impl Direction {
    fn reverse(&self) -> Self {
        match self {
            Direction::Up => Direction::Down,
            Direction::Down => Direction::Up,
            Direction::Right => Direction::Left,
            Direction::Left => Direction::Right,
        }
    }
}

#[derive(Debug)]
struct Map {
    squares: Vec<Square>,
    ncols: usize,
    nrows: usize,
}

fn bounds_check(place: usize, forward: bool, size: usize) -> usize {
    if place == 0 && !forward {
        return size - 1;
    }
    if place == size - 1 && forward {
        return 0;
    }

    if forward {
        return place + 1;
    } else {
        return place - 1;
    }
}

impl Map {
    fn at(&self, row: usize, col: usize) -> Square {
        assert!(row < self.nrows);
        assert!(col < self.ncols);
        self.squares[row * self.ncols + col]
    }

    fn step(&self, row: usize, col: usize, dir: Direction) -> (usize, usize) {
        match dir {
            Direction::Up => (bounds_check(row, false, self.nrows), col),
            Direction::Down => (bounds_check(row, true, self.nrows), col),
            Direction::Right => (row, bounds_check(col, true, self.ncols)),
            Direction::Left => (row, bounds_check(col, false, self.ncols)),
        }
    }

    fn next(&self, row: usize, col: usize, dir: Direction) -> Option<(usize, usize)> {
        // Return None if we hit a wall
        let (next_r, next_c) = self.step(row, col, dir);

        let next_square = self.at(next_r, next_c);
        match next_square {
            Square::Wall => None,
            Square::Empty => {
                // Reverse direction, move until we hit an empty square
                let reverse_dir = dir.reverse();
                let mut current_coords = (row, col);
                let mut current_cell = self.at(current_coords.0, current_coords.1);
                assert!(current_cell == Square::Open);
                while current_cell != Square::Empty {
                    current_coords = self.step(current_coords.0, current_coords.1, reverse_dir);
                    current_cell = self.at(current_coords.0, current_coords.1);
                }

                // Step in original direction to get to non-empty square
                current_coords = self.step(current_coords.0, current_coords.1, dir);
                current_cell = self.at(current_coords.0, current_coords.1);

                match current_cell {
                    Square::Wall => None,
                    Square::Empty => panic!(
                        "Shouldn't be possible at loc:\n{}",
                        self.show_w_mark(current_coords)
                    ),
                    Square::Open => Some(current_coords),
                }
            }
            Square::Open => Some((next_r, next_c)),
        }
    }

    fn from_string(s: &str) -> Self {
        let lines = s.lines().collect::<Vec<&str>>();
        let ncols = lines.iter().map(|l| l.len()).max().unwrap();
        let nrows = lines.len();
        let mut squares = Vec::new();
        for line in lines {
            let mut map_line = Vec::new();
            for c in line.chars() {
                match c {
                    '.' => map_line.push(Square::Open),
                    '#' => map_line.push(Square::Wall),
                    ' ' => map_line.push(Square::Empty),
                    _ => panic!("Invalid character"),
                }
            }
            while map_line.len() < ncols {
                map_line.push(Square::Empty);
            }

            squares.extend(map_line);
        }

        Self {
            squares,
            ncols,
            nrows,
        }
    }

    fn show(&self) -> String {
        return self.show_lines().join("\n");
    }

    fn show_w_mark(&self, loc: (usize, usize)) -> String {
        let mut lines = self.show_lines();
        let mut line = lines[loc.0].clone();
        line.replace_range(loc.1..loc.1 + 1, "X");
        lines[loc.0] = line;
        return lines.join("\n");
    }

    fn show_lines(&self) -> Vec<String> {
        let mut lines = Vec::new();
        for row in 0..self.nrows {
            let mut line = String::new();
            for col in 0..self.ncols {
                let square = self.at(row, col);
                match square {
                    Square::Empty => line.push(' '),
                    Square::Open => line.push('.'),
                    Square::Wall => line.push('#'),
                }
            }
            lines.push(line);
        }
        return lines;
    }
}

#[derive(Debug, PartialEq)]
enum Instruction {
    TurnLeft,
    TurnRight,
    Step(usize),
}
use Instruction::*;

impl Instruction {
    fn from_string(s: &str) -> Vec<Instruction> {
        let s = s.trim();
        let mut start = 0;
        let mut pointer = 0;
        let mut instructions = Vec::new();
        let chars = s.chars().collect::<Vec<char>>();

        while pointer < chars.len() {
            let mut parsing_step = false;
            while pointer < chars.len() && chars[pointer].is_numeric() {
                parsing_step = true;
                pointer += 1;
            }
            // Pointer now points to first non-numeric char

            if parsing_step {
                let substring = &s[start..pointer];
                let step = substring.parse::<usize>().unwrap();
                instructions.push(Step(step));
                start = pointer;
                continue;
            }

            let instruction = match chars[pointer] {
                'L' => TurnLeft,
                'R' => TurnRight,
                _ => panic!("Invalid instruction: '{}'", chars[pointer]),
            };
            instructions.push(instruction);
            pointer += 1;
            start = pointer;
        }

        return instructions;
    }
}

struct Walker {
    map: Map,
    instructions: Vec<Instruction>,
    history: HashMap<(usize, usize), Direction>, // For debugging,
    location: (usize, usize),
    facing: Direction,
}

impl Walker {
    fn new(map: Map, instructions: Vec<Instruction>) -> Self {
        let mut location = (0, 0);
        while map.at(location.0, location.1) != Square::Open {
            location = map.step(location.0, location.1, Direction::Right);
        }

        let instructions = instructions.into_iter().rev().collect::<Vec<Instruction>>();
        let mut history = HashMap::new();
        let facing = Direction::Right;
        history.insert(location, facing);

        Self {
            map,
            instructions,
            history,
            location,
            facing,
        }
    }

    fn step_all(&mut self) {
        while self.instructions.len() > 0 {
            self.step();
        }
    }

    fn step(&mut self) {
        let instruction = self.instructions.pop().expect("Stack underflow");
        match instruction {
            TurnLeft => {
                self.facing = match self.facing {
                    Direction::Up => Direction::Left,
                    Direction::Down => Direction::Right,
                    Direction::Right => Direction::Up,
                    Direction::Left => Direction::Down,
                };
                self.history.insert(self.location, self.facing);
            }
            TurnRight => {
                self.facing = match self.facing {
                    Direction::Up => Direction::Right,
                    Direction::Down => Direction::Left,
                    Direction::Right => Direction::Down,
                    Direction::Left => Direction::Up,
                };
                self.history.insert(self.location, self.facing);
            }
            Step(n) => {
                for _ in 0..n {
                    self.step_one();
                    self.history.insert(self.location, self.facing);
                }
            }
        }
    }

    fn step_one(&mut self) {
        let next = self.map.next(self.location.0, self.location.1, self.facing);
        match next {
            None => {}
            Some((r, c)) => {
                self.location = (r, c);
            }
        }
    }

    fn show(&self) -> String {
        let mut lines = self.map.show_lines();
        for (loc, fac) in self.history.iter() {
            let (row, col) = loc;

            let c = match fac {
                Direction::Up => '^',
                Direction::Down => 'v',
                Direction::Left => '<',
                Direction::Right => '>',
            };

            let line = lines.get_mut(*row).unwrap();
            let mut chars = line.chars().collect::<Vec<char>>();
            chars[*col] = c;
            *line = chars.into_iter().collect::<String>();
        }
        return lines.join("\n");
    }

    fn from_string(s: &str) -> Self {
        let parts = s.split("\n\n").collect::<Vec<&str>>();
        let map = Map::from_string(parts[0]);
        let instructions = Instruction::from_string(parts[1]);
        Self::new(map, instructions)
    }

    fn to_password(&self) -> usize {
        let facing_num = match self.facing {
            Direction::Right => 0,
            Direction::Down => 1,
            Direction::Left => 2,
            Direction::Up => 3,
        };

        let password = 1000 * (self.location.0 + 1) + 4 * (self.location.1 + 1) + facing_num;
        return password;
    }
}

fn main() {
    let filename = "src/input.txt";
    let contents =
        std::fs::read_to_string(filename).expect(&format!("Failed to read '{filename}'"));
    let mut walker = Walker::from_string(&contents);
    walker.step_all();
    // println!("{}", walker.show());
    // println!("{:?}", walker.location);
    println!("Password: {}", walker.to_password());
}
