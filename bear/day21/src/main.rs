use std::cell::RefCell;
use std::collections::HashMap;
use std::env;

type Value = f64;

#[derive(Debug, Clone)]
enum Op {
    Plus,
    Minus,
    Div,
    Mul,
    RootOp,
}

impl Op {
    fn from_string(s: &str) -> Op {
        match s {
            "+" => Op::Plus,
            "-" => Op::Minus,
            "/" => Op::Div,
            "*" => Op::Mul,
            _ => panic!("Unknown operator {}", s),
        }
    }
}

type Name = String;

#[derive(Debug, Clone)]
enum Monkey {
    Number(Value),
    Operator {
        op: Op,
        lhs_name: Name,
        rhs_name: Name,
    },
}

impl Monkey {
    fn eval(&self, monkeys: &HashMap<Name, Monkey>) -> Value {
        use Monkey::*;
        match self {
            Number(x) => *x,
            Operator {
                op,
                lhs_name,
                rhs_name,
            } => {
                let lhs = monkeys
                    .get(lhs_name)
                    .expect(&format!("No monkey named {}", lhs_name));
                let rhs = monkeys
                    .get(rhs_name)
                    .expect(&format!("No monkey named {}", rhs_name));
                let lhs = lhs.eval(monkeys);
                let rhs = rhs.eval(monkeys);
                match op {
                    Op::Plus => lhs + rhs,
                    Op::Minus => lhs - rhs,
                    Op::Div => lhs / rhs,
                    Op::Mul => lhs * rhs,
                    Op::RootOp => (lhs - rhs).abs(),
                }
            }
        }
    }

    fn from_string(s: &str, is_part2: bool) -> (Name, Self) {
        let name_monkey_s = s.split(":").map(|s| s.trim()).collect::<Vec<_>>();
        assert_eq!(name_monkey_s.len(), 2);

        let name = name_monkey_s[0];
        let monkey_s = name_monkey_s[1];

        let try_num = monkey_s.parse::<Value>();
        if let Ok(num) = try_num {
            return (name.to_string(), Monkey::Number(num));
        } else {
            let parts = monkey_s.split(" ").map(|s| s.trim()).collect::<Vec<_>>();
            assert_eq!(parts.len(), 3);
            let lhs_name = parts[0];
            let rhs_name = parts[2];
            let op = if is_part2 && name == "root" {
                Op::RootOp
            } else {
                Op::from_string(parts[1])
            };
            return (
                name.to_string(),
                Monkey::Operator {
                    op,
                    lhs_name: lhs_name.to_string(),
                    rhs_name: rhs_name.to_string(),
                },
            );
        }
    }
}

fn read_monkeys(file_contents: &str, is_part2: bool) -> HashMap<Name, Monkey> {
    let lines = file_contents
        .split("\n")
        .filter(|x| x.len() > 0)
        .collect::<Vec<_>>();

    let mut monkeys = HashMap::new();
    for line in lines {
        let (name, monkey) = Monkey::from_string(line, is_part2);
        monkeys.insert(name, monkey);
    }

    return monkeys;
}

fn part1(file_contents: String) {
    let monkeys_map = read_monkeys(&file_contents, false);
    let root_value = monkeys_map
        .get("root")
        .expect("No root monkey!")
        .eval(&monkeys_map);

    println!("Christmas is very late: {root_value}");
}

fn nelder_mead(x1: Value, eval_fn: impl Fn(Value) -> Value) -> Value {
    let mut x1 = x1;
    let mut x2 = x1 + 10.0;

    let mut y1 = eval_fn(x1);
    let mut y2 = eval_fn(x2);

    let max_iter = 10000;
    for _ in 0..max_iter {
        if y1 == 0.0 {
            return x1;
        }
        if y2 == 0.0 {
            return x2;
        }

        if y2 < y1 {
            let temp = x1;
            x1 = x2;
            x2 = temp;
            let temp = y1;
            y1 = y2;
            y2 = temp;
        }
        assert!(y1 <= y2);

        let xo = x1;

        // Reflection
        let xr = xo + (xo - x2);
        let yr = eval_fn(xr);
        if yr < y1 {
            let xe = xo + 2.0 * (xo - x2);
            let ye = eval_fn(xe);
            if ye < yr {
                x2 = xe;
                y2 = ye;
            } else {
                x2 = xr;
                y2 = yr;
            }
            continue;
        }

        // Contraction
        if yr < y2 {
            let xc = xo + (xr - xo) / 2.0;
            let yc = eval_fn(xc);
            if yc < yr {
                x2 = xc;
                y2 = yc;
                continue;
            }
        }
        if yr >= y2 {
            let xc = xo + (x2 - xo) / 2.0;
            let yc = eval_fn(xc);
            if yc < y2 {
                x2 = xc;
                y2 = yc;
                continue;
            }
        }

        // Shrink
        x2 = x1 + (x2 - x1) / 2.0;
        y2 = eval_fn(x2);
    }

    panic!("Didn't converge!");
}

fn part2(file_contents: String) {
    let monkeys_map = RefCell::new(read_monkeys(&file_contents, true));

    let eval_fn = |x: Value| {
        let mut monkeys_map = monkeys_map.borrow_mut();
        monkeys_map.insert("humn".to_string(), Monkey::Number(x));
        let root_value = monkeys_map
            .get("root")
            .expect("No root monkey!")
            .eval(&monkeys_map);
        root_value
    };

    let result = nelder_mead(0.0, eval_fn);
    let result_value = eval_fn(result);

    if result_value != 0.0 {
        panic!("Couldn't find a point where the result is 0!");
    } else {
        println!("Part 2 Solution was {result} (evaluated to {result_value})");
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let do_part2 = args.iter().find(|s| s == &"--part2").is_some();
    let live = args.iter().find(|s| s == &"--live").is_some();
    let filename = if live { &args[1] } else { "../src/test.txt" };

    let file_contents =
        std::fs::read_to_string(filename).expect(&format!("Failed to read file {filename}"));
    if do_part2 {
        part2(file_contents);
    } else {
        part1(file_contents);
    }
}
