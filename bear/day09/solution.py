

def parse_tuple(line):
    direction, n_steps = line.strip().split(" ")
    return direction, int(n_steps)


def parse_input(fname):
    with open(fname, "r") as f:
        lines = f.read().split("\n")

    return [parse_tuple(line) for line in lines if len(line) > 0]


def clamp(x, x0, x1):
    if x < x0:
        return x0
    elif x > x1:
        return x1
    else:
        return x


def update_tail(head_loc, tail_loc):
    dx = head_loc[0] - tail_loc[0]
    dy = head_loc[1] - tail_loc[1]

    if abs(dx) <= 1 and abs(dy) <= 1:
        return
    else:
        tail_loc[0] += clamp(dx, -1, 1)
        tail_loc[1] += clamp(dy, -1, 1)


def part1(debug=False):
    fname = "test.txt" if debug else "input.txt"
    instructions = parse_input(fname)


    head_loc = [0, 0]
    tail_loc = [0, 0]
    visited_tail_locs = set()
    for inst in instructions:
        match inst:
            case ("U", n):
                def updater():
                    head_loc[1] += 1
            case ("R", n):
                def updater():
                    head_loc[0] += 1
            case ("L", n):
                def updater():
                    head_loc[0] -= 1
            case ("D", n):
                def updater():
                    head_loc[1] -= 1
            case x:
                raise Exception(f"Unknown instruction {inst}")
        for i in range(n):
            updater()
            update_tail(head_loc, tail_loc)
            visited_tail_locs.add((tail_loc[0], tail_loc[1]))

    print(f"Number of visited locations: {len(visited_tail_locs)}")




part1()


def update_tails(locs):
    for i in range(len(locs) - 1):
        dx = locs[i][0] - locs[i + 1][0]
        dy = locs[i][1] - locs[i + 1][1]

        if abs(dx) <= 1 and abs(dy) <= 1:
            continue
        else:
            locs[i + 1][0] += clamp(dx, -1, 1)
            locs[i + 1][1] += clamp(dy, -1, 1)



def part2(debug=False):
    fname = "test2.txt" if debug else "input.txt"
    instructions = parse_input(fname)


    # Now locs has both head and tails
    locs = []
    for i in range(10):
        locs.append([0, 0])

    visited_tail_locs = set()
    for inst in instructions:
        match inst:
            case ("U", n):
                def updater():
                    locs[0][1] += 1
            case ("R", n):
                def updater():
                    locs[0][0] += 1
            case ("L", n):
                def updater():
                    locs[0][0] -= 1
            case ("D", n):
                def updater():
                    locs[0][1] -= 1
            case x:
                raise Exception(f"Unknown instruction {inst}")
        for i in range(n):
            updater()
            update_tails(locs)
            visited_tail_locs.add((locs[9][0], locs[9][1]))

    print(f"Number of visited locations: {len(visited_tail_locs)}")




part2()

print("I did not copy paste this")
print("       __    ")
print("    .-'  |   ")
print("   /   <\|   ")
print("  /     \'   ")
print("  |_.- o-o   ")
print("  / C  -._)\ ")
print(" /',        |")
print("|   `-,_,__,'")
print("(,,)====[_]=|")
print("  '.   ____/ ")
print("   | -|-|_   ")
print("   |____)_)  ")
print("             ")
