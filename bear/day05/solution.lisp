(require :uiop)

(defun read-lines (filename)
  (with-open-file (h filename)
    (loop for line = (read-line h nil nil)
          while line
          collect line)))



(defun take (n ls)
  (loop for i below n
        for x = (nth i ls)
        while x
        collect x))


(defun drop (n ls)
  (nthcdr n ls))
        

(defun group-n (n ls)
  (let ((times (+ 1 (floor (/ (length ls) n)))))
    (loop for i below times
          for els = (take n (drop (* i n) ls))
          while els
          collect els)))


(defun eq-to-bad-char? (char)
  (or
   (equal char #\ )
   (equal char #\[)
   (equal char #\])))



(defun take-stacks (input)
  (loop for line in input
        for groups = (group-n 4 (coerce line 'list))
        while (some (lambda (ls) (member #\[ ls)) groups)
        collect (mapcar (lambda (ls) (remove-if #'eq-to-bad-char? ls)) groups)))


(defun make-start-stacks (input)
  (let* ((number-of-stacks (length (car input)))
         (stacks (loop for i below number-of-stacks collect nil)))
    (loop for row in input
          do (loop for i = 0 then (+ i 1)
                   for el in row
                   do (when el (push el (nth i stacks))))
          )
    (mapcar #'reverse stacks)))


(defun parse-move (move-line)
  (mapcar
   (lambda (pairlist)
     (list (read-from-string (car pairlist))
           (parse-integer (cadr pairlist))))
   (group-n 2 (uiop:split-string move-line))))



(defun drop-until (p ls)
  (let ((index (loop for i = 0 then (+ i 1)
                     for el = (nth i ls)
                     while (and el (not (funcall p el)))
                     finally (return i))))
    (drop (+ 1 index) ls)))


(defun parse-input (input)
  (let ((data (make-hash-table)))
    (setf (gethash 'stacks data) (make-start-stacks (take-stacks input)))
    (setf (gethash 'moves data)
          (mapcar #'parse-move (drop-until (lambda (line) (= (length line) 0)) input)))
  data))


(defmacro aget (key place)
  `(second (assoc ,key ,place)))


(defun run-instruction (stacks inst)
  (let ((number (aget 'move inst))
        (src    (- (aget 'from inst) 1))
        (dst     (- (aget 'to inst) 1)))
    (dotimes (i number)
      (push (pop (nth src stacks)) (nth dst stacks))))
  stacks)


(defun run-instructions (runner data)
  (let ((stacks (gethash 'stacks data))
        (moves (gethash 'moves data)))
    (loop for move in moves
          do (funcall runner stacks move))))


(defun take-top (data)
  (let ((stacks (gethash 'stacks data)))
    (coerce (loop for stack in stacks
          collect (car (car stack))) 'string)))


(defun part-1 ()
  (let* ((input (read-lines "input.txt"))
         (data (parse-input input)))
    (run-instructions #'run-instruction data)
    (take-top data)))



(defun run-instruction-9001 (stacks inst)
  (let* ((number (aget 'move inst))
         (src-loc (- (aget 'from inst) 1))
         (dst-loc (- (aget 'to inst) 1))
         (moved (reverse (take number (nth src-loc stacks)))))
    (dotimes (i number)
      (push (nth i moved) (nth dst-loc stacks))
      (pop (nth src-loc stacks)))))


(defun part-2 ()
  (let* ((input (read-lines "input.txt"))
         (data (parse-input input)))
    (run-instructions #'run-instruction-9001 data)
    (take-top data)))


(format t "Part 1 answer: ~a~%" (part-1))
(format t "Part 2 answer: ~a~%" (part-2))
(format t "It's christmas alright~%")

