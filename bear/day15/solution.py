from pathlib import Path
from time import time
import numpy as np

def parse_coord(str):
    str = str.strip().replace("x=", "").replace("y=",  "")

    return int(str)

def parse_pos(pos_str):
    x, y = pos_str.strip().split(", ")

    return (parse_coord(x), parse_coord(y))


def parse_sensor(line):
    pos, beac = line.replace("Sensor at ", "").replace("closest beacon is at", "").split(":")

    return (parse_pos(pos), parse_pos(beac))

def read_sensors(fname):
    lines = [l for l in Path(fname).read_text().split("\n") if len(l) > 0]

    return [parse_sensor(l) for l in lines]


def manhattan_dist(x1, y1, x2, y2):

    return abs(x1 - x2) + abs(y1 - y2)

def manhattan_radius(sensor):
    pt, beac = sensor

    return manhattan_dist(*pt, *beac)

def count_empty(sensors, row_number):
    beacons = set()

    empty_segs = []
    for i, sensor in enumerate(sensors):
        radius = manhattan_radius(sensor)
        beacons.add(sensor[1])
        x, y = sensor[0]

        if abs(row_number - y) > radius:
            continue

        range_reduction = abs(y - row_number)

        search_radius = radius - range_reduction

        empty_segs.append((x - search_radius, x + search_radius))
    
    go = True
    while go:
        go = False
        pop_index = None
        for i in range(len(empty_segs)):
            x11, x12 = empty_segs[i]
            for j in range(len(empty_segs)):
                if i == j:
                    continue

                x21, x22 = empty_segs[j]

                if (x21 <= x11 and x11 <= x22) or (x21 <= x12 and x12 <= x22) or (x11 <= x21 and x21 <= x12):
                    go = True
                    pop_index = j
                    x11 = min(x11, x21)
                    x12 = max(x12, x22)
                    empty_segs[i] = (x11, x12)
                    break
            if go:
                break
        if go:
            empty_segs.pop(pop_index)
                

    count = 0
    for x1, x2 in empty_segs:
        local_count = x2 - x1 + 1
        beac_count = sum([1 for beac in beacons
                        if beac[1] == row_number and x1 <= beac[0] and beac[0] <= x2])
        count += local_count - beac_count

    return count


def part1(debug=False):
    fname = "test.txt" if debug else "input.txt"

    sensors = read_sensors(fname)

    row_number = 10 if debug else 2_000_000
    n_empty = count_empty(sensors, row_number)

    print(f"{n_empty} squares at row {row_number}")

part1()


def count_empty_2(sensors, row_number, max_coord):
    beacons = set()

    empty_segs = []
    for i, sensor in enumerate(sensors):
        radius = manhattan_radius(sensor)
        beacons.add(sensor[1])
        x, y = sensor[0]

        if abs(row_number - y) > radius:
            continue

        range_reduction = abs(y - row_number)

        search_radius = radius - range_reduction

        empty_segs.append((x - search_radius, x + search_radius))

    go = True
    while go:
        go = False
        pop_index = None
        for i in range(len(empty_segs)):
            x11, x12 = empty_segs[i]
            for j in range(len(empty_segs)):
                if i == j:
                    continue

                x21, x22 = empty_segs[j]

                if (x21 <= x11 and x11 <= x22) or (x21 <= x12 and x12 <= x22) or (x11 <= x21 and x21 <= x12):
                    go = True
                    pop_index = j
                    x11 = min(x11, x21)
                    x12 = max(x12, x22)
                    empty_segs[i] = (x11, x12)
                    break
            if go:
                break
        if go:
            empty_segs.pop(pop_index)
                

    # Possible locations indicated by a list of segments:
    locs = [(0, max_coord)]
    for x1, x2 in empty_segs:
        locs = remove_subinterval(locs, (x1, x2))

    for beac in beacons:
        if beac[1] != row_number or beac[0] < 0 or beac[0] > max_coord:
            continue
        locs = remove_subinterval(locs, (beac[0], beac[0]))

    free_spaces, first_free = count_free_spaces(locs)

    return free_spaces, first_free

def remove_subinterval(intervals, sub):
    new_inters = []

    sx1, sx2 = sub
    for x1, x2 in intervals:
        if sx1 <= x1 and x2 <= sx2:
            # This interval is completely contained in sub and we remove it
            continue
        elif sx1 <= x1 and x1 <= sx2:
            # Partially contained, add a part with sub intersection removed
            new_inter = (sx2 + 1, x2)
            new_inters.append(new_inter)
        elif sx1 <= x2 and x2 <= sx2:
            new_inter = (x1, sx1-1)
            new_inters.append(new_inter)
        elif x1 < sx1 and sx2 < x2:
            # sub is within interval and we need to split it
            new1 = (x1, sx1 - 1)
            new2 = (sx2 + 1, x2)
            new_inters.append(new1)
            new_inters.append(new2)
        elif x1 == sx1 and sx2 < x2:
            new_inters.append((sx1+1, x2))
        elif x1 < sx1 and sx2 == x2:
            new_inters.append((x1, sx1-1))

    return new_inters

def count_free_spaces(intervals):
    count = 0
    first_free = None
    for x1, x2 in intervals:
        count += x2 - x1 + 1
        first_free = x1
    return count, first_free





def part2(debug=False):
    fname = "test.txt" if debug else "input.txt"

    sensors = read_sensors(fname)

    dx = 20 if debug else 4_000_000

    for i in range(dx):
        count, coord = count_empty_2(sensors, i, dx)

        if count == 1:
            print(f"Beacon is at {coord, i}!")
            print(f"Tuning frequency: {coord * 4_000_000 + i}")
            break
        print(f"Processed {i+1} rows", end="\r")

part2()



# radius = manhattan_radius(((8, 7), (2, 10)))
# for y in range(-3, 20+1):
#     for x in range(-2, 25+1):
#         if manhattan_dist(x, y, 8, 7) <= radius:
#             print("#", end="")
#         else:
#             print(".", end="")
#     print()