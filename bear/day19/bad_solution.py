"""
ore -> ore robot
ore -> clay robot
ore + clay -> obsidian robot
ore + obsidian -> geode robot

"""
import re
from dataclasses import dataclass, asdict
from typing import Tuple, List, Dict
from pathlib import Path
from argparse import ArgumentParser

parser = ArgumentParser()
parser.add_argument("-d", action="store_true")

args = parser.parse_args()


@dataclass
class Blueprint:
    ID: int
    or_cost: int
    cr_cost: int
    obr_cost: Tuple[int, int]
    gr_cost: Tuple[int, int]

def read_blueprint(line):
    exp = r"Blueprint ([0-9]+): Each ore robot costs ([0-9]+) ore. Each clay robot costs ([0-9]+) ore. Each obsidian robot costs ([0-9]+) ore and ([0-9]+) clay. Each geode robot costs ([0-9]+) ore and ([0-9]+) obsidian."

    result = re.match(exp, line)
    groups = result.groups()
    ID        = int(groups[0])
    or_cost   = int(groups[1])
    cr_cost   = int(groups[2])
    obr_cost  = (int(groups[3]), int(groups[4]))
    gr_cost   = (int(groups[5]), int(groups[6]))

    return Blueprint(ID, or_cost, cr_cost, obr_cost, gr_cost)


def read_file(fname):
    txt = Path(fname).read_text()

    return [read_blueprint(line) for line in txt.split("\n") if len(line) > 0]



@dataclass(unsafe_hash=True)
class State:
    ore_robots   : int
    clay_robots  : int
    obs_robots   : int
    geo_robots   : int

    ore_pending  : int
    clay_pending : int
    obs_pending  : int
    geo_pending  : int
    
    ore          : int
    clay         : int
    obs          : int
    geo          : int

    time         : int

    def copy(self):
        return State(self.ore_robots, self.clay_robots, self.obs_robots, self.geo_robots,
                     self.ore_pending, self.clay_pending, self.obs_pending, self.geo_pending,
                     self.ore, self.clay, self.obs, self.geo,
                     self.time)
                     


def get_new_states(state, blueprint) -> List[State]:
    max_ore = max([blueprint.or_cost, blueprint.cr_cost,
                   blueprint.obr_cost[0], blueprint.gr_cost[0]])
    max_clay = blueprint.obr_cost[1]
    max_obs = blueprint.gr_cost[1]
    
    possible_states = []
    if state.ore >= blueprint.obr_cost[0] and state.clay >= blueprint.obr_cost[1] and state.obs_robots < max_obs:
        new_state = state.copy()
        new_state.obs_pending += 1
        new_state.ore -= blueprint.obr_cost[0]
        new_state.clay -= blueprint.obr_cost[1]
        possible_states.append(new_state)
    if state.ore >= blueprint.or_cost and state.ore_robots < max_ore:
        new_state = state.copy()
        new_state.ore_pending += 1
        new_state.ore -= blueprint.or_cost
        possible_states.append(new_state)
    if state.ore >= blueprint.cr_cost and state.clay_robots < max_clay:
        new_state = state.copy()
        new_state.clay_pending += 1
        new_state.ore -= blueprint.cr_cost
        possible_states.append(new_state)
    if state.ore >= blueprint.gr_cost[0] and state.obs >= blueprint.gr_cost[1]:
        new_state = state.copy()
        new_state.geo_pending += 1
        new_state.ore -= blueprint.gr_cost[0]
        new_state.obs -= blueprint.gr_cost[1]
        possible_states.append(new_state)

    return possible_states[::-1]


def update_state(state):
    # state = state.copy()

    state.time += 1
    state.ore += state.ore_robots
    state.clay += state.clay_robots
    state.obs += state.obs_robots
    state.geo += state.geo_robots

    state.ore_robots += state.ore_pending
    state.clay_robots += state.clay_pending
    state.obs_robots += state.obs_pending
    state.geo_robots += state.geo_pending

    state.ore_pending = 0
    state.clay_pending = 0
    state.obs_pending = 0
    state.geo_pending = 0

    return state

# Check this https://github.com/ephemient/aoc2022/blob/main/py/aoc2022/day19.py
from collections import defaultdict

def max_potential(bp, state, time):
    additional_robots = defaultdict(int)
    potential_materials = {"ore": state.ore,
                           "clay": state.clay,
                           "obs": state.obs,
                           "geo": state.geo}
    for i in range(time):
        potential_materials["ore"] += state.ore_robots + additional_robots["ore"]
        potential_materials["clay"] += state.clay_robots + additional_robots["clay"]
        potential_materials["obs"] += state.obs_robots + additional_robots["obs"]
        potential_materials["geo"] += state.geo_robots + additional_robots["geo"]

        if potential_materials["ore"] >= bp.or_cost * (additional_robots["ore"] + 1):
            additional_robots["ore"] += 1
        if potential_materials["ore"] >= bp.cr_cost * (additional_robots["clay"] + 1):
            additional_robots["clay"] += 1
        if potential_materials["ore"] >= bp.obr_cost[0] * (additional_robots["obs"] + 1) and potential_materials["clay"] >= bp.obr_cost[1] * (additional_robots["obs"] + 1):
            additional_robots["obs"] += 1
        if potential_materials["ore"] >= bp.gr_cost[0] * (additional_robots["geo"] + 1) and potential_materials["obs"] >= bp.gr_cost[1] * (additional_robots["geo"] + 1):
            additional_robots["geo"] += 1

        if i == 0:
            additional_robots["ore"] += state.ore_pending
            additional_robots["clay"] += state.clay_pending
            additional_robots["obs"] += state.obs_pending
            additional_robots["geo"] += state.geo_pending
                             

    return potential_materials["geo"]

    
def find_bp_max(bp: Blueprint) -> int:
    time_limit = 24

    state = State(1, 0, 0, 0,
                  0, 0, 0, 0,
                  0, 0, 0, 0,
                  0)
    # Try some kind of DFS-like search
    # with elimination of solutions that are definitely worse
    stack = [state]

    bests_so_far = set()
    candidates = set()

    best_val = -1

    i = 0

    while len(stack) > 0:
        print(f"Tested {i} states -- {len(bests_so_far)=} -- {len(candidates)=} -- {best_val=}   ", end="\r")
        i += 1
        state = stack.pop()

        # if any(strictly_better(other_state, state)
        #        for other_state in bests_so_far):
        #     continue

        if max_potential(bp, state, 24 - state.time) < best_val:
            continue

        if state.time == 24:
            # candidates.add(state)
            # if state.geo >= 9:
            #     raise Exception(f"{state}")
            best_val = max(best_val, state.geo)
            continue

        # bests_so_far.add(state)
        # bests_so_far = set(
        #     filter(lambda o_state: not strictly_worse(o_state, state), bests_so_far))


        # state = update_state(state)
        # print(state.time, ":", state)
        # if state.time == 4:
        #     continue
        # state = update_state(state)
        new_states = get_new_states(state, bp)
        # while True:
        #     new_new_states = [state2 for state in new_states[-1]
        #                       for state2 in get_new_states(state, bp)]
        #     if len(new_new_states) == 0:
        #         break
        #     new_states.append(new_new_states)

        # new_states = [state for ls in new_states for state in ls]

        new_states.insert(0, state.copy())

        for new_state in new_states:
            update_state(new_state)


        for state in new_states:
            stack.append(state)
    print(f"Tested {i} states -- {len(bests_so_far)=} -- {len(candidates)=} -- {best_val=}")
            
    return best_val #max(candidates, key=lambda state: state.geo)
    

def part1(debug):
    fname = "test.txt" if debug else "input.txt"

    bps = read_file(fname)
    # print("max pot for bp0:", max_potential(bps[0], State(1, 0, 0, 0,
    #                                                       0, 0, 0, 0,
    #                                                       0, 0, 0, 0,
    #                                                       0), 24))

    for bp in bps:
        print(find_bp_max(bp))
    # print(bps[1])
    # bp_max = find_bp_max(bps[1])
    # print(bp_max)



part1(args.d)
    
