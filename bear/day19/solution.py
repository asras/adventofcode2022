import re
from dataclasses import dataclass, asdict
from typing import Tuple, List, Dict, Any
from argparse import ArgumentParser
from pathlib import Path
from multiprocessing import Pool
from functools import reduce

parser = ArgumentParser()
parser.add_argument("-d", action="store_true")
parser.add_argument("-t", action="store_true")
parser.add_argument("-p", action="store_true")
parser.add_argument("--concurrent", action="store_true")

args = parser.parse_args()

@dataclass
class Blueprint:
    ID: int
    or_cost: int
    cr_cost: int
    obr_cost: Tuple[int, int]
    gr_cost: Tuple[int, int]

    def max_ore(self):
        return max(self.or_cost, self.cr_cost, self.obr_cost[0], self.gr_cost[0])

    def max_clay(self):
        return self.obr_cost[1]

    def max_obs(self):
        return self.gr_cost[1]

@dataclass(unsafe_hash=True)
class State:
    ore_robots   : int
    clay_robots  : int
    obs_robots   : int
    geo_robots   : int

    ore          : int
    clay         : int
    obs          : int
    geo          : int

    time         : int

    def copy(self):
        return State(self.ore_robots, self.clay_robots, self.obs_robots, self.geo_robots,
                     self.ore, self.clay, self.obs, self.geo,
                     self.time)

    def update(self, nturns):
        assert nturns >= 0, nturns
        nturns = min(nturns, self.time)
        
        self.ore += self.ore_robots * nturns
        self.clay += self.clay_robots * nturns
        self.obs += self.obs_robots * nturns
        self.geo += self.geo_robots * nturns
        self.time -= nturns
        assert self.time >= 0



def read_blueprint(line):
    exp = r"Blueprint ([0-9]+): Each ore robot costs ([0-9]+) ore. Each clay robot costs ([0-9]+) ore. Each obsidian robot costs ([0-9]+) ore and ([0-9]+) clay. Each geode robot costs ([0-9]+) ore and ([0-9]+) obsidian."

    result = re.match(exp, line)
    groups = result.groups()
    ID        = int(groups[0])
    or_cost   = int(groups[1])
    cr_cost   = int(groups[2])
    obr_cost  = (int(groups[3]), int(groups[4]))
    gr_cost   = (int(groups[5]), int(groups[6]))

    return Blueprint(ID, or_cost, cr_cost, obr_cost, gr_cost)


def read_file(fname):
    txt = Path(fname).read_text()

    return [read_blueprint(line) for line in txt.split("\n") if len(line) > 0]


def next_ore_robot_when(bp, state):
    missing_ore = max(0, bp.or_cost - state.ore)
    missing_turns = missing_ore // state.ore_robots + ((missing_ore % state.ore_robots) > 0)

    return missing_turns
    

def next_clay_robot_when(bp, state):
    missing_ore = max(0, bp.cr_cost - state.ore)
    missing_turns = missing_ore // state.ore_robots + ((missing_ore % state.ore_robots) > 0)

    return missing_turns


def next_obs_robot_when(bp, state):
    if state.clay_robots == 0:
        return 100_000
    
    missing_ore = max(0, bp.obr_cost[0] - state.ore)
    missing_clay = max(0, bp.obr_cost[1] - state.clay)
    missing_ore_turns = missing_ore // state.ore_robots + ((missing_ore % state.ore_robots) > 0)
    missing_clay_turns = missing_clay // state.clay_robots + ((missing_clay % state.clay_robots) > 0)

    return max(missing_ore_turns, missing_clay_turns)


def next_geo_robot_when(bp, state):
    if state.obs_robots == 0:
        return 100_000
    
    missing_ore = max(0, bp.gr_cost[0] - state.ore)
    missing_obs = max(0, bp.gr_cost[1] - state.obs)
    missing_ore_turns = missing_ore // state.ore_robots + ((missing_ore % state.ore_robots) > 0)
    missing_obs_turns = missing_obs // state.obs_robots + ((missing_obs % state.obs_robots) > 0)

    return max(missing_ore_turns, missing_obs_turns)



def purchase(robot_name, bp, state):
    match robot_name:
        case "or":
            state.ore -= bp.or_cost
            assert state.ore >= 0
            state.ore_robots += 1
        case "cr":
            state.ore -= bp.cr_cost
            assert state.ore >= 0
            state.clay_robots += 1
        case "obr":
            state.ore -= bp.obr_cost[0]
            state.clay -= bp.obr_cost[1]
            assert state.ore >= 0
            assert state.clay >= 0
            state.obs_robots += 1
        case "gr":
            state.ore -= bp.gr_cost[0]
            state.obs -= bp.gr_cost[1]
            assert state.ore >= 0
            assert state.obs >= 0
            state.geo_robots += 1



def max_potential(bp, state):
    state = state.copy()
    starts = {"ore": state.ore_robots,
              "clay": state.clay_robots,
              "obs": state.obs_robots,
              "geo": state.geo_robots}

    for t in range(state.time):
        state.update(1)

        n_ore = (state.ore_robots - starts["ore"] + 1)
        if state.ore >= bp.or_cost * n_ore:
            state.ore_robots += 1

        n_clay = state.clay_robots - starts["clay"] + 1
        if state.ore >= bp.cr_cost * n_clay:
            state.clay_robots += 1

        n_obs = state.obs_robots + 1 - starts["obs"]
        if state.ore >= bp.obr_cost[0] * n_obs and state.clay >= bp.obr_cost[1] * n_obs:
            state.obs_robots += 1

        n_geo = state.geo_robots + 1 - starts["geo"]
        if state.ore >= bp.gr_cost[0] * n_geo and state.obs >= bp.gr_cost[1] * n_geo:
            state.geo_robots += 1
            
    assert state.time == 0
    return state.geo



def find_bp_max(bp, time_limit, verbose=False):
    stack = [State(1, 0, 0, 0,
                   0, 0, 0, 0,
                   time_limit)]

    nstates = 0
    best_val = 0
    best_state = None
    while len(stack) > 0:
        if verbose:
            print(f"Checked {nstates} states", end="\r")
        nstates += 1
        state = stack.pop()

        if state.time == 0:
            if state.geo >= best_val:
                best_state = state
            best_val = max(best_val, state.geo)
            continue


        if max_potential(bp, state) < best_val:
            continue

        estimate = state.geo + state.geo_robots * state.time
        if estimate > best_val:
            best_val = estimate


        
        # Next time to build an ore robot
        if state.ore_robots < bp.max_ore():
            missing_turns = next_ore_robot_when(bp, state)
            if missing_turns < state.time:
                new_state = state.copy()
                new_state.update(missing_turns + 1)
                purchase("or", bp, new_state)
                stack.append(new_state)
        # Next time to build a clay robot
        if state.clay_robots < bp.max_clay():
            missing_turns = next_clay_robot_when(bp, state)
            if missing_turns < state.time:
                new_state = state.copy()
                new_state.update(missing_turns + 1)
                purchase("cr", bp, new_state)
                stack.append(new_state)
        # Next time to build an obs robot
        if state.obs_robots < bp.max_obs():
            missing_turns = next_obs_robot_when(bp, state)
            if missing_turns < state.time:
                new_state = state.copy()
                new_state.update(missing_turns + 1)
                purchase("obr", bp, new_state)
                stack.append(new_state)
        # Next time to build a geo robot
        missing_turns = next_geo_robot_when(bp, state)
        if missing_turns < state.time:
            new_state = state.copy()
            new_state.update(missing_turns + 1)
            purchase("gr", bp, new_state)
            stack.append(new_state)

    if verbose:
        print(f"Checked {nstates} states")
    
    return best_val
        

def test():
    bp = read_file("test.txt")[0]
    state = State(1, 0, 0, 0,
                  0, 0, 0, 0,
                  24)

    n = next_clay_robot_when(bp, state)
    state.update(n + 1)
    purchase("cr", bp, state)
    assert 24 - state.time == 3
    assert state.ore == 1
    assert state.ore_robots == 1
    assert state.clay_robots == 1

    n = next_clay_robot_when(bp, state)
    state.update(n + 1)
    purchase("cr", bp, state)
    assert 24 - state.time == 5
    assert state.ore == 1
    assert state.clay == 2
    assert state.ore_robots == 1
    assert state.clay_robots == 2

    n = next_clay_robot_when(bp, state)
    state.update(n + 1)
    purchase("cr", bp, state)
    assert 24 - state.time == 7
    assert state.ore == 1
    assert state.clay == 6
    assert state.ore_robots == 1
    assert state.clay_robots == 3

    n = next_obs_robot_when(bp, state)
    state.update(n + 1)
    purchase("obr", bp, state)
    assert 24 - state.time == 11
    assert state.ore == 2
    assert state.clay == 4
    assert state.ore_robots == 1
    assert state.clay_robots == 3
    assert state.obs_robots == 1

    n = next_clay_robot_when(bp, state)
    state.update(n + 1)
    purchase("cr", bp, state)
    assert 24 - state.time == 12, state
    assert state.ore == 1
    assert state.clay == 7
    assert state.clay_robots == 4
    assert state.obs == 1
    assert state.obs_robots == 1

    n = next_obs_robot_when(bp, state)
    state.update(n + 1)
    purchase("obr", bp, state)
    assert 24 - state.time == 15
    assert state.ore == 1
    assert state.clay == 5
    assert state.ore_robots == 1
    assert state.clay_robots == 4
    assert state.obs_robots == 2



    n = next_geo_robot_when(bp, state)
    state.update(n + 1)
    purchase("gr", bp, state)
    assert 24 - state.time == 18
    assert state.ore == 2
    assert state.clay == 17
    assert state.obs == 3
    assert state.geo == 0


    n = next_geo_robot_when(bp, state)
    state.update(n + 1)
    purchase("gr", bp, state)
    assert 24 - state.time == 21
    assert state.ore == 3
    assert state.clay == 29
    assert state.obs == 2
    assert state.geo == 3
    
    n = next_geo_robot_when(bp, state)
    assert n + 1 > state.time
    state.update(n + 1)
    assert 24 - state.time == 24, state
    assert state.ore == 6, state
    assert state.clay == 41
    assert state.obs == 8
    assert state.geo == 9

    print("TESTED PASSED")


if args.t:
    test()
    exit()


fname = "test.txt" if args.d else "input.txt"
time_limit = 32 if args.p else 24

bps = read_file(fname)
bps = bps[:3] if args.p else bps

if args.concurrent:
    def helper(i):
        bpmax = find_bp_max(bps[i], time_limit, verbose=False)
        if args.p:
            return bpmax
        else:
            return bps[i].ID * bpmax
    
    with Pool(len(bps)) as p:
        results = p.map(helper, list(range(len(bps))))


    if args.p:
        print(f"Result: {reduce(lambda a, b: a*b, results)}")
    else:
        print(f"Result: {sum(results)}")

else:
    qual = 1 if args.p else 0
    for bp in bps:
        bpmax = find_bp_max(bp, time_limit, verbose=False) 
        if args.p:
            qual *= bpmax
        else:
            qual += bp.ID * bpmax

    print(f"Qual: {qual}")
    


    
