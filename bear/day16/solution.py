from pathlib import Path
from functools import cache
from decimate import find_path, decimate
import re
import numpy as np


def read_line(line):
    reg = r"Valve ([A-Z]{2}) has flow rate=([0-9]+); tunnel(s?) lead(s?) to valve(s?) ([A-Z]{2})((, [A-Z]{2})*)"
    
    matches = re.search(reg, line)

    v_name = matches.groups()[0]
    flow_rate = int(matches.groups()[1])
    
    connections = [matches.groups()[5]] 
    if matches.groups()[6] is not None:
        connections += [s for s in matches.groups()[6].split(", ") if len(s) > 0]

    return v_name, flow_rate, connections

def read_valves(fname):
    lines = Path(fname).read_text().split("\n")

    network = {}
    for i, line in enumerate(lines):
        name, fr, conns = read_line(line)
        
        network[name] = (i, fr, conns)

    return network

from argparse import ArgumentParser
parser = ArgumentParser()
parser.add_argument("-d", action="store_true")

args = parser.parse_args()

fname = "test.txt" if args.d else "input.txt"

network = read_valves(fname)

#(dist, path) = find_path("AA", "AA", network)
#print(f"{dist=}")
#print(f"{path=}")

#decimate(network)

#exit()

global_cache = {}
cache_gets = 0
def path_cache(fn):
    global global_cache
    
    def wrapped(x, y, p, z):
        if (x, y, z) in global_cache:
            global cache_gets
            cache_gets += 1
            return global_cache[(x, y, z)]
        else:
            value = fn(x, y, p, z)
            if value is None:
                return 0
            global_cache[(x, y, z)] = value
            return value

    return wrapped


useful_nodes = []
for k, (idx, fr, _) in network.items():
    if fr > 0:       
        useful_nodes.append((k, idx))


useful_nodes_flag = 0
for name, (idx, fr, _) in network.items():
    if fr > 0:
        useful_nodes_flag |= (1 << idx)


def targets_at(node, minutes_left, actions_available):
    if actions_available & useful_nodes_flag != 0:
        new_nodes = [n for n in useful_nodes if n != node]
        for new_node, idx in new_nodes:
            if new_node == node: continue
            dist, path = find_path(node, new_node, network)
            if dist < minutes_left and ((actions_available >> idx) & 1) == 1:
                yield new_node, dist
        

    


abort_mission =0
@cache
def solve(node, minutes_left, actions_available):
    if minutes_left <= 0:
        return 0
    
    idx, fr, conns = network[node]
    if idx in path:
        return None

    valve_closed = ((actions_available >> idx) & 1) == 1

    if valve_closed and fr > 0:
        # Two options: skip this valve or open it

        # Skip
        skip_max = max([solve(target, minutes_left - time, actions_available)
                        for target, time in targets_at(node, minutes_left, actions_available)], default=0)

        # Open
        # Set valve flag to zero
        new_actions = actions_available ^ (1 << idx)
        assert ((new_actions >> idx) & 1) == 0
        open_max = fr * (minutes_left-1) + max([solve(target, minutes_left - 1 - time, new_actions)
                                                for target, time in targets_at(node, minutes_left, new_actions)], default=0)

        return max(open_max, skip_max)
    else:
        other_sols = [solve(target, minutes_left - time, actions_available)
                        for target, time in targets_at(node, minutes_left, actions_available)]
        return max(other_sols, default=0)
        


n_nodes = len(network.keys())
all_actions_available = 0
for i in range(n_nodes):
    all_actions_available |= (1 << i)

#print(all_actions_available)
#solution = solve("AA", 10, all_actions_available)
#solution = solve("AA", 12, all_actions_available)
idx = network["AA"][0]
path = []
solution = solve("AA", 30, all_actions_available)
print(solution)
print("Aborted mission:", abort_mission)
CALLCNT = 0


def actions(node1, node2, actions_available):
    idx1, fr1, conns1 = network[node1]
    idx2, fr2, conns2 = network[node2]

    closed1 = ((actions_available >> idx1) & 1) == 1
    closed2 = ((actions_available >> idx2) & 1) == 1


    # Both leave
    for c1 in conns1:
        if c1 == node2:
            continue
        for c2 in conns2:
            yield (c1, 0, c2, 0, actions_available)

    if closed1 and closed2:
        # Case where both leave is already covered
        if fr1 > 0:
            if fr2 > 0:
                # Both stay
                if idx1 != idx2:
                    new_acts = (actions_available ^ (1 << idx1)) ^ (1 << idx2)
                    assert ((new_acts >> idx1) & 1) == 0
                    assert ((new_acts >> idx2) & 1) == 0
                    yield (node1, fr1, node2, fr2, new_acts)
            # 1 stays
            new_acts = (actions_available ^ (1 << idx1))
            assert ((new_acts >> idx1) & 1) == 0
            for conn in conns2:
                yield (node1, fr1, conn, 0, new_acts)
        if fr2 > 0:
            # 2 stays
            new_acts = (actions_available) ^ (1 << idx2)
            assert ((new_acts >> idx2) & 1) == 0
            for conn in conns1:
                yield (conn, 0, node2, fr2, new_acts)
    elif closed1 and not closed2:
        # 2 will always leave we only need to cover case where 1 stays
        if fr1 > 0:
            # 1 stays
            new_acts = (actions_available ^ (1 << idx1))
            assert ((new_acts >> idx1) & 1) == 0
            for conn in conns2:
                yield (node1, fr1, conn, 0, new_acts)
    elif not closed1 and closed2:
        # 1 will always leave we only need to cover case where 2 stays
        new_acts = (actions_available) ^ (1 << idx2)
        assert ((new_acts >> idx2) & 1) == 0
        if fr2 > 0:
            for conn in conns1:
                yield (conn, 0, node2, fr2, new_acts)


def single_actions(node, actions_available):
    idx1, fr1, conns1 = network[node]
    
    closed = ((actions_available >> idx1) & 1) == 1

    if closed and fr1 > 0:
        new_acts = (actions_available ^ (1 << idx1))
        assert ((new_acts >> idx1) & 1) == 0
        yield node, fr1, new_acts
    else:
        for conn in conns1:
            yield conn, 0, actions_available
    

def count_bits(n):
    cnt = 0
    while n:
        cnt += n & 1
        n >>= 1
    return cnt


@cache
def solve_2_trick(node, minutes_left, actions_available):
    if minutes_left <= 0:
        return solve("AA", 26, actions_available)
    num_actions = count_bits(actions_available & useful_nodes_flag)
    if num_actions <= len(useful_nodes) // 2:
        return solve("AA", 26, actions_available)
    
    
    idx, fr, conns = network[node]
    if idx in path:
        return None

    valve_closed = ((actions_available >> idx) & 1) == 1

    if valve_closed and fr > 0:
        # Two options: skip this valve or open it

        # Skip
        skip_max = max([solve_2_trick(target, minutes_left - time, actions_available)
                        for target, time in targets_at(node, minutes_left, actions_available)], default=0)

        # Open
        # Set valve flag to zero
        new_actions = actions_available ^ (1 << idx)
        assert ((new_actions >> idx) & 1) == 0
        open_max = fr * (minutes_left-1) + max([solve_2_trick(target, minutes_left - 1 - time, new_actions)
                                                for target, time in targets_at(node, minutes_left, new_actions)], default=0)

        return max(open_max, skip_max)
    else:
        other_sols = [solve_2_trick(target, minutes_left - time, actions_available)
                        for target, time in targets_at(node, minutes_left, actions_available)]
        return max(other_sols, default=0)

print(solve_2_trick("AA",26, all_actions_available))
exit()


def solve_2_dfs():
    stack = [("AA", "AA", all_actions_available, 0, 0, 0, [])]
    bests = {}

    best_sol = 0
    history = None
    most_fr = 0
    best_left = 0
    while len(stack) > 0:
        nnode, enode, actions_available, fr, flow, time, hist = stack.pop()
        if time > 0:
            extra = (25 - time) * network[nnode][1] + (25-time) * network[enode][1]
            if (nnode, enode, time) not in bests:
                bests[(nnode, enode, time)] = (flow, fr)
            else:
                f, frr = bests[(nnode, enode, time)]
                expected = f + (26 - time) * frr
                if expected < (flow + (26 - time) * fr) + extra:
                    bests[(nnode, enode, time)] = (flow, fr)
                else:
                    continue
            
        
        if flow > best_sol:
            best_sol = flow
            best_left = actions_available
            history = hist
        
        #best_sol = max(best_sol, flow)

        if time >= 26:
            continue

        time += 1
        flow += fr
        most_fr = max(fr, most_fr)
        

        #for (n1, g1, n2, g2, next_actions) in actions(nnode, enode, actions_available):
        for n1, g1, next_actions in single_actions(nnode, actions_available):
            for n2, g2, actions in single_actions(enode, next_actions):
                n_fr = fr + g1 + g2
                new_hist = hist.copy()
                new_hist.append((time, flow, fr, n1 if g1 > 0 else "", n2 if g2 > 0 else ""))
        
                if (n1, n2, time) not in bests:
                    stack.append((n1, n2, actions, n_fr, flow, time, new_hist))
                else:
                    f, frr = bests[(n1, n2, time)]
                    expected = f + (26 - time) * frr
                    extra = (25 - time) * network[n1][1] + (25-time) * network[n2][1]
                    if expected < (flow + (26 - time) * n_fr) + extra:
                        stack.append((n1, n2, actions, n_fr, flow, time, new_hist))
                

    print("keep it flowing", most_fr)
    print("actions", count_bits(best_left & useful_nodes_flag))
    for time, f, fr, op1, op2 in history:
        print(time, f, fr, op1, op2)
    return best_sol


print("DFS:",  solve_2_dfs())
#print("Tricky solution:", solve_2_trick("AA", 26, all_actions_available))
exit()

# Smart caching
# X Y Z W = Y X Z W

cache_gets = 0

global_cache = {}
def smart_cache(fn):
    global global_cache
    
    def wrapped(x, y, px, py, z, w):
        global cache_gets
        if (x, y, px, py, z, w) in global_cache:        
            cache_gets += 1
            return global_cache[(x, y, px, py, z, w)]
        elif (y, x, py, px, z, w) in global_cache:
            cache_gets += 1
            return global_cache[(y, x, py, px, z, w)]
        else:
            value = fn(x, y, px, py, z, w)
            global_cache[(x, y, px, py, z, w)] = value
            return value

    return wrapped


def targets_at2(node, e_node, minutes_left, my_time, elephant_time, actions_available):
    if my_time < elephant_time:
        yield node, e_node, elephant_time, elephant_time
        return
    elif elephant_time < my_time:
        yield node, e_node, my_time, my_time
        return
    else:
        if actions_available & useful_nodes_flag != 0:
            new_nodes = [n for n in useful_nodes if n != node]
            for new_node, idx in new_nodes:
                if new_node == node: continue
                dist, path = find_path(node, new_node, network)
                for e_new_node, e_idx in new_nodes:
                    e_dist, _ = find_path(e_node, e_new_node, network)
                    if dist < minutes_left and ((actions_available >> idx) & 1) == 1 and e_dist < minutes_left and ((actions_available >> e_idx) & 1) == 1:
                        yield new_node, e_new_node, my_time + dist, elephant_time + dist


def actions(node, time, actions_available):
    if actions_available & useful_nodes_flag != 0:
        new_nodes = [n for n in useful_nodes if n != node]
        for new_node, idx in new_nodes:
            if new_node == node: continue
            dist, path = find_path(node, new_node, network)
            if dist + time < 26:
                yield new_node, time + dist

def solve222(node, enode, time, etime, actions_available):
    mtime = 7
    if time >= mtime and etime >= mtime:
        return 0

    if time <= etime:
        # take action for node
        idx, fr, conns = network[node]
        valve_closed = ((actions_available >> idx) & 1) == 1

        sol = 0
        if valve_closed and node != "AA":
            assert (useful_nodes_flag >> idx) & 1 == 1
            new_actions = actions_available ^ (1 << idx)
            val = fr * (mtime - time) + solve222(node, enode, time+1, etime, new_actions)
            sol = max(sol, val)
        
        for nnode, ntime in actions(node, time, actions_available):
            val = solve222(nnode, enode, ntime, etime, actions_available)
            sol = max(sol, val)

        return sol
    elif etime < time:
        # take action for enode
        idx, fr, conns = network[enode]
        valve_closed = ((actions_available >> idx) & 1) == 1

        sol = 0
        if valve_closed and enode != "AA":
            assert (useful_nodes_flag >> idx) & 1 == 1
            new_actions = actions_available ^ (1 << idx)
            val = fr * (mtime -  etime) + solve222(node, enode, time, etime+1, new_actions)
            sol = max(sol, val)
        
        for nnode, ntime in actions(enode, etime, actions_available):
            val = solve222(node, nnode, time, ntime, actions_available)
            sol = max(sol, val)

        return sol
    

print(solve222("AA", "AA", 0, 0, all_actions_available))
exit()



def solve22(node, e_node, time, e_time, minutes_left, actions_available):
    idx, fr, conns = network[node]
    e_idx, e_fr, e_conns = network[e_node]

    valve_closed = ((actions_available >> idx) & 1) == 1
    elephant_closed = ((actions_available >> idx) & 1) == 1
    if node == e_node:
        # Send elephant to next node, I do one of two actions
        
        if valve_closed and fr > 0:
            # Two options: skip this valve or open it

            # Skip
            skip_max = 0
            for nn, enn, mt, et in targets_at2(node, e_node, minutes_left, time, e_time, actions_available):
                if nn == enn:
                    continue
                sol = solve2(nn,  enn, mt,  et, minutes_left-max(mt, et), actions_available)
                skip_max = max(skip_max, sol)
            

            # Open
            # Set valve flag to zero
            new_actions = actions_available ^ (1 << idx)
            assert ((new_actions >> idx) & 1) == 0
            open_max = fr * (minutes_left-1) + max([solve2(node, e_conn, node, elephant_node, minutes_left - 1, new_actions)
                                                for e_conn in e_conns
                                                if e_conn != prev_e_node], default=0)

            return max(open_max, skip_max)
        else:
            other_sols = [solve2(conn, e_conn, node, elephant_node, minutes_left - 1, actions_available) 
                            for i, conn in enumerate(conns)
                            for e_conn in e_conns[i:]
                            if conn != prev_node and e_conn != prev_e_node and conn != prev_e_node and e_conn != prev_e_node]
            return max(other_sols, default=0)
    else:
        max_sol = -1
        for (my_node, my_gain, e_node, e_gain, new_acts) in actions(node, elephant_node, actions_available):
            if my_node == prev_node or e_node == prev_e_node or my_node == prev_e_node or e_node == prev_node:
                continue
            sol = my_gain * (minutes_left - 1) + e_gain * (minutes_left - 1) + solve2(my_node, e_node, node, elephant_node, minutes_left - 1, new_acts)
            max_sol = max(max_sol, sol)
        return max_sol
    sol = 0
    for nn, enn, mt, et in targets_at2(node, e_node, minutes_left, time, e_time, actions_available):
        l_sol = solve2(nn, enn, mt, et, minutes_left-max(mt, et), )

    
    if time != e_time:
        raise Exception("Out of sync")
    
    if time >= 26:
        assert minutes_left <= 0
        return 0

@smart_cache
def solve2(node, elephant_node, prev_node, prev_e_node, minutes_left, actions_available):
    #global global_cache
    #    print(f"Cache size: {len(global_cache)}", end="\r")
    #global cache_gets
    #print("gets:", cache_gets, "size:", len(global_cache), end="\r")
    # global CALLCNT
    # CALLCNT += 1
    # print(f"{CALLCNT=}", end="\r")
    if minutes_left <= 0:
        return 0

    if (actions_available & useful_nodes) == 0:
        print("Aborting mission")
        return 0
    
    idx, fr, conns = network[node]
    e_idx, e_fr, e_conns = network[elephant_node]

    valve_closed = ((actions_available >> idx) & 1) == 1
    elephant_closed = ((actions_available >> idx) & 1) == 1

    if node == elephant_node:
        # Send elephant to next node, I do one of two actions
        
        if valve_closed and fr > 0:
            # Two options: skip this valve or open it

            # Skip
            skip_max = max([solve2(conn, e_conn, node, elephant_node, minutes_left - 1, actions_available)
                                for conn in conns
                                for e_conn in e_conns
                                if conn != prev_node and e_conn != prev_e_node], default=0)

            # Open
            # Set valve flag to zero
            new_actions = actions_available ^ (1 << idx)
            assert ((new_actions >> idx) & 1) == 0
            open_max = fr * (minutes_left-1) + max([solve2(node, e_conn, node, elephant_node, minutes_left - 1, new_actions)
                                                for e_conn in e_conns
                                                if e_conn != prev_e_node], default=0)

            return max(open_max, skip_max)
        else:
            other_sols = [solve2(conn, e_conn, node, elephant_node, minutes_left - 1, actions_available) 
                            for i, conn in enumerate(conns)
                            for e_conn in e_conns[i:]
                            if conn != prev_node and e_conn != prev_e_node and conn != prev_e_node and e_conn != prev_e_node]
            return max(other_sols, default=0)
    else:
        max_sol = -1
        for (my_node, my_gain, e_node, e_gain, new_acts) in actions(node, elephant_node, actions_available):
            if my_node == prev_node or e_node == prev_e_node or my_node == prev_e_node or e_node == prev_node:
                continue
            sol = my_gain * (minutes_left - 1) + e_gain * (minutes_left - 1) + solve2(my_node, e_node, node, elephant_node, minutes_left - 1, new_acts)
            max_sol = max(max_sol, sol)
        return max_sol
        


def actions(node1, node2, actions_available):
    idx1, fr1, conns1 = network[node1]
    idx2, fr2, conns2 = network[node2]

    closed1 = ((actions_available >> idx1) & 1) == 1
    closed2 = ((actions_available >> idx2) & 1) == 1


    # Both leave
    for c1 in conns1:
        if c1 == node2:
            continue
        for c2 in conns2:
            yield (c1, 0, c2, 0, actions_available)

    if closed1 and closed2:
        # Case where both leave is already covered
        if fr1 > 0:
            if fr2 > 0:
                # Both stay
                new_acts = (actions_available ^ (1 << idx1)) ^ (1 << idx2)
                yield (node1, fr1, node2, fr2, new_acts)
            # 1 stays
            new_acts = (actions_available ^ (1 << idx1))
            for conn in conns2:
                yield (node1, fr1, conn, 0, new_acts)
        if fr2 > 0:
            # 2 stays
            new_acts = (actions_available) ^ (1 << idx2)
            for conn in conns1:
                yield (conn, 0, node2, fr2, new_acts)
    elif closed1 and not closed2:
        # 2 will always leave we only need to cover case where 1 stays
        if fr1 > 0:
            # 1 stays
            new_acts = (actions_available ^ (1 << idx1))
            for conn in conns2:
                yield (node1, fr1, conn, 0, new_acts)
    elif not closed1 and closed2:
        # 1 will always leave we only need to cover case where 2 stays
        new_acts = (actions_available) ^ (1 << idx2)
        if fr2 > 0:
            for conn in conns1:
                yield (conn, 0, node2, fr2, new_acts)
            


sol2 = solve2("AA", "AA", -1, -1, 26, all_actions_available)
print()
print(f"{sol2=}")
# print(solve("AA", 26, all_actions_available))