
dijsktra_cache = {}

def dijsktra(fn):

    def wrapped(s, g, n):
        global dijsktra_cache
        if (s, g) in dijsktra_cache:
            return dijsktra_cache[(s, g)]
        else:
            val = fn(s, g, n)
            dijsktra_cache[(s, g)] = val
            return val

    return wrapped

    
@dijsktra
def find_path(start, goal, network):
    unvisited = set(x for x in network)
    distances =  {x: (2**20, []) for x in network}
    distances[start] = (0, [start])

    queue = [(k, [start]) for k in network[start]]

    while len(unvisited) > 0:
        #(next, path) = queue.pop(0)
        for next in network[start][2]:
            if next not in unvisited:
                continue

            tent_dist = distances[start][0] + 1
            if distances[next][0] > tent_dist:
                new_path = distances[start][1].copy()
                new_path.append(next)
                distances[next] = (tent_dist, new_path)
        
        unvisited.remove(start)
        min_dist = 2**20
        for k, (dist, _) in distances.items():
            if dist < min_dist and k in unvisited:
                min_dist = dist
                start = k
                
    return distances[goal]


def decimate(network):
    useful_nodes = []
    for k, (_, fr, _) in network.items():
        if fr > 0:            
            useful_nodes.append(k)

    necessary_nodes = set()

    for n1 in useful_nodes:
        for n2 in useful_nodes:
            if n1 == n2:
                continue
                
            (dist, path) = find_path(n1, n2, network)

            for n in path:
                necessary_nodes.add(n)
        
    print("Start size:", len(network.keys()))
    print("Decimated size: ", len(necessary_nodes))



