
from functools import cache

def count_bits(n):
    cnt = 0
    while n:
        cnt += n & 1
        n >>= 1
    return cnt


@cache
def solve_2_trick(node, minutes_left, actions_available):
    if minutes_left <= 0:
        return solve("AA", 26, actions_available)
    num_actions = count_bits(actions_available & useful_nodes_flag)
    if num_actions <= len(useful_nodes) // 2:
        return solve("AA", 26, actions_available)
    
    
    idx, fr, conns = network[node]
    if idx in path:
        return None

    valve_closed = ((actions_available >> idx) & 1) == 1

    if valve_closed and fr > 0:
        # Two options: skip this valve or open it

        # Skip
        skip_max = max([solve_2_trick(target, minutes_left - time, actions_available)
                        for target, time in targets_at(node, minutes_left, actions_available)], default=0)

        # Open
        # Set valve flag to zero
        new_actions = actions_available ^ (1 << idx)
        assert ((new_actions >> idx) & 1) == 0
        open_max = fr * (minutes_left-1) + max([solve_2_trick(target, minutes_left - 1 - time, new_actions)
                                                for target, time in targets_at(node, minutes_left, new_actions)], default=0)

        return max(open_max, skip_max)
    else:
        other_sols = [solve_2_trick(target, minutes_left - time, actions_available)
                        for target, time in targets_at(node, minutes_left, actions_available)]
        return max(other_sols, default=0)