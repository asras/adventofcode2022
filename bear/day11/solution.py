from typing import List, Callable
from dataclasses import dataclass


@dataclass
class Monkey:
    m_id: int
    items: List[int]
    operation: Callable[int, int]
    test: Callable[int, bool]
    true_target: int
    false_target: int
    inspect_count: int

    def test_print(self, old, test_val):
        print(f"{self.m_id=}")
        print(f"{self.items=}")
        print(f"Operation on {old=}: {self.operation(old)}")
        print(f"Test on {test_val=}: {self.test(test_val)}")
        print(f"{self.true_target=}")
        print(f"{self.false_target=}")

    
def parse_monkey(m_str: str) -> Monkey:
    lines = m_str.split("\n")
    m_id = int(lines[0].replace("Monkey ", "").replace(":", "").strip())
    items = [int(n.strip())
             for n in lines[1].strip().split(": ")[1].split(", ")]
    operation_str = lines[2].split(": ")[1].split(" = ")[1].strip()
    operation = lambda old : eval(operation_str)
    test_val = int(lines[3].split(": ")[1].replace("divisible by ", ""))
    test = lambda val: val % test_val == 0
    true_target = int(lines[4].split(": ")[1].replace("throw to monkey ", ""))
    false_target = int(lines[5].split(": ")[1].replace("throw to monkey ", ""))

    return Monkey(m_id, items, operation, test, true_target, false_target, 0)
    

def read_monkeys(fname: str) -> List[Monkey]:
    with open(fname, "r") as f:
        monkey_strs = [m for m in f.read().split("\n\n") if len(m) > 0]

    return [parse_monkey(monkey_str) for monkey_str in monkey_strs]



def monkey_round(monkeys, is_relaxed=True):
    for monkey in monkeys:
        for item in monkey.items:
            # Worry that monkey is inspecting
            monkey.inspect_count += 1
            item = monkey.operation(item)
            # Decrease worry because monkey didnt break item
            if is_relaxed:
                item = item // 3
            # Test
            test_res = monkey.test(item)
            # Select throw target
            target = monkey.true_target if test_res else monkey.false_target
            monkeys[target].items.append(item)

        # Monkey threw all items so empty its list
        monkey.items = []


def find_master_inspectors(monkeys):
    max_inspect = (-1, -1)

    for monkey in monkeys:
        if monkey.inspect_count > max_inspect[0]:
            max_inspect = (monkey.inspect_count, max_inspect[0])
        elif monkey.inspect_count > max_inspect[1]:
            max_inspect = (max_inspect[0], monkey.inspect_count)

    return max_inspect

def part1(debug=False):
    fname = "test.txt" if debug else "input.txt"
    monkeys = read_monkeys(fname)

    for i in range(20):
        monkey_round(monkeys)

    a, b = find_master_inspectors(monkeys)
    print(f"Inspector product: {a * b}")

    
part1()        


