from typing import List, Callable, Tuple
from dataclasses import dataclass
from functools import reduce


@dataclass
class Monkey:
    m_id: int
    items: List[int]
    operation: Callable[int, int]
    test: int
    true_target: int
    false_target: int
    inspect_count: int

    def test_print(self, old, test_val):
        print(f"{self.m_id=}")
        print(f"{self.items=}")
        print(f"Operation on {old=}: {self.operation(old)}")
        print(f"Test on {test_val=}: {self.test(test_val)}")
        print(f"{self.true_target=}")
        print(f"{self.false_target=}")

    
def parse_monkey(m_str: str) -> Monkey:
    lines = m_str.split("\n")
    m_id = int(lines[0].replace("Monkey ", "").replace(":", "").strip())
    items = [int(n.strip())
             for n in lines[1].strip().split(": ")[1].split(", ")]
    operation_str = lines[2].split(": ")[1].split(" = ")[1].strip()
    operation = eval(f"lambda old: {operation_str}")
    test_val = int(lines[3].split(": ")[1].replace("divisible by ", ""))
    test = test_val
    true_target = int(lines[4].split(": ")[1].replace("throw to monkey ", ""))
    false_target = int(lines[5].split(": ")[1].replace("throw to monkey ", ""))

    return Monkey(m_id, items, operation, test, true_target, false_target, 0)
    

def read_monkeys(fname: str) -> Tuple[List[Monkey], List[int], int]:
    with open(fname, "r") as f:
        monkey_strs = [m for m in f.read().split("\n\n") if len(m) > 0]

    monkeys = [parse_monkey(monkey_str) for monkey_str in monkey_strs]

    item_count = sum([len(monkey.items) for monkey in monkeys])
    item_worry_levels = [item for monkey in monkeys for item in monkey.items]

    item_index = 0
    for i, monkey in enumerate(monkeys):
        new_items = [0] * item_count
        for j in range(len(monkey.items)):
            new_items[item_index] = 1
            item_index += 1

        monkey.items = new_items

    max_worry = reduce(lambda a, b: a * b, [monkey.test for monkey in monkeys])                  

    return monkeys, item_worry_levels, max_worry
            



monkeys, item_worrys, max_worry = read_monkeys("test.txt")


def monkey_round(monkeys, item_worrys, max_worry):
    for monkey in monkeys:
        for item_index, item in enumerate(monkey.items):
            if not item:
                continue

            # Lookup item worry
            item_worry = item_worrys[item_index]

            # Worry that monkey is inspecting
            monkey.inspect_count += 1
            item_worry = monkey.operation(item_worry) % max_worry
            # Update worry list
            item_worrys[item_index] = item_worry

            # Test
            test_res = item_worry % monkey.test == 0

            # Select throw target
            target = monkey.true_target if test_res else monkey.false_target
            monkeys[target].items[item_index] = 1
            monkey.items[item_index] = 0


def find_master_inspectors(monkeys):
    max_inspect = (-1, -1)

    for monkey in monkeys:
        if monkey.inspect_count > max_inspect[0]:
            max_inspect = (monkey.inspect_count, max_inspect[0])
        elif monkey.inspect_count > max_inspect[1]:
            max_inspect = (max_inspect[0], monkey.inspect_count)

    return max_inspect


def part2(debug=False):
    fname = "test.txt" if debug else "input.txt"
    monkeys, item_worrys, max_worry = read_monkeys(fname)

    for i in range(10000):
        monkey_round(monkeys, item_worrys, max_worry)

    a, b = find_master_inspectors(monkeys)
    print(f"Inspector product: {a * b}")


part2(False)
