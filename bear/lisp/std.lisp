(require :uiop)

(defun read-lines (filename)
  (with-open-file (h filename)
    (loop for line = (read-line h nil nil)
          while line
          collect line)))

(defun make-sym (str)
  (read-from-string str))

(defun take (n ls)
  (loop for i below n
        for x = (nth i ls)
        while x
        collect x))


(defun drop (n ls)
  (nthcdr n ls))
        

(defun group-n (n ls)
  (let ((times (+ 1 (floor (/ (length ls) n)))))
    (loop for i below times
          for els = (take n (drop (* i n) ls))
          while els
          collect els)))

(defun drop-until (p ls)
  (let ((index (loop for i = 0 then (+ i 1)
                     for el = (nth i ls)
                     while (and el (not (funcall p el)))
                     finally (return i))))
    (drop (+ 1 index) ls)))

(defun split (str s)
  (uiop:split-string str :separator s))


(defun startswith (s str)
  (let* ((prelen (length s))
         (str-pre (subseq str 0 prelen)))
    (string-equal s str-pre)))
    
    


