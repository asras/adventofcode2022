import Data.Char (ord)
import Data.Set (Set)
import qualified Data.Set as S


charToPrio :: Char -> Int
charToPrio ch
  | ord ch >= ord 'a' = ord ch - ord 'a' + 1
  | ord ch <= ord 'Z' = ord ch - ord 'A' + 27


bisect :: [a] -> ([a], [a])
bisect ls = (take half ls, drop half ls)
  where
    half = length ls `div` 2


commonItems :: Ord a => ([a], [a]) -> Set a
commonItems (ls1, ls2) = go ls1 ls2 (S.empty)
  where
    go [] _ acc = acc
    go _ [] acc = acc
    go (el1:rem1) ls acc
      | el1 `elem` ls = go rem1 ls (S.insert el1 acc)
      | otherwise = go rem1 ls acc


-- pipe operator
(|>) x f = f x


groupN :: Int -> [a] -> [[a]]
groupN n ls = go n ls []
  where
    go n [] acc = reverse acc
    go n ls acc = go n (drop n ls) (take n ls : acc)


badgeSearch :: Ord a => [[a]] -> Set a
badgeSearch ls = foldr S.intersection startSet $ map S.fromList $ drop 1 ls
  where
    startSet = S.fromList $ head ls

main = do
  rucksacks <- lines <$> readFile "input.txt"

  let process rucksack = rucksack |> bisect |> commonItems |> S.toList |> head |> charToPrio
  putStr "Part 1: "
  putStrLn $ show $ sum $ map process rucksacks

  let processBadges elfGroup = elfGroup |> badgeSearch |> S.toList |> head |> charToPrio
  putStr "Part 2: "
  putStrLn $ show $ sum $ map processBadges $ groupN 3 rucksacks

  putStrLn $ "Christmas has come!"


  putStrLn $ "           *        "  
  putStrLn $ "          /#\\       "   
  putStrLn $ "         /###\\      "     
  putStrLn $ "        /#####\\     "      
  putStrLn $ "       /#######\\    "     
  putStrLn $ "      /#########\\   "      
  putStrLn $ "     /###########\\  "      
  putStrLn $ "          ||        "     
  putStrLn $ "                    "
