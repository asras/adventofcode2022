(load "../lisp/std.lisp")


(defparameter test1 '("mjqjpqmgbljsphdztnvjfqwrcgsmlb" 7))
(defparameter test2 '("bvwbjplbgvbhsrlpgdmjqwftvncz" 5))
(defparameter test3 '("nppdvjthqldpwncqszvftbrmjlhg" 6))
(defparameter test4 '("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg" 10))
(defparameter test5 '("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw" 11))

(defun run-test (test)
  (= (get-start-marker (car test)) (cadr test)))

(defun get-start-marker (input rep-length)
  (let ((length (length input)))
    (loop for pointer below length
          for chars = (subseq input pointer (+ pointer rep-length))
          while (any-repeated chars)
          finally (return (+ pointer rep-length)))))

(defun any-repeated (chars)
  (duplicates? (coerce chars 'list)))
  
(defun duplicates? (ls)
  (when ls
    (or
     (member (car ls) (cdr ls))
     (any-repeated (cdr ls)))))
  

(defun part-1 ()
  (let ((input (car (read-lines "input.txt"))))
    (get-start-marker input 4)))

(format t "The answer to the most fantastic part 1 is: ~a ~%" (part-1))

(defun part-2 ()
  (let ((input (car (read-lines "input.txt"))))
    (get-start-marker input 14)))

(format t "The answer to the most amazing part 2 is: ~a ~%" (part-2))
