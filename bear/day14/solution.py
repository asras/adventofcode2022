from pathlib import Path
from typing import List, Tuple, Set
from dataclasses import dataclass

@dataclass
class Point:
    x: int
    y: int

    def copy(self):
        return Point(self.x, self.y)

    def __hash__(self):
        return self.x * 10_000_000 + self.y


def range_it(v1, v2) -> List[int]:
    return list(range(min(v1, v2), max(v1, v2) + 1))

def between(x, x1, x2):
    minx = min(x1, x2)
    maxx = max(x1, x2)
    
    return minx <= x and x <= maxx

@dataclass
class Geometry:
    segments: List[Tuple[Point, Point]]
    bbox: Tuple[Point, Point]

    def make_x_range(self, sand, with_floor=False):
        minx = min(self.bbox[0].x, min([pt.x for pt in sand], default=self.bbox[0].x))
        maxx = max(self.bbox[1].x, max([pt.x for pt in sand], default=self.bbox[1].x))
        if with_floor:
            x_range = list(range(minx - 2, maxx + 1 + 2))
        else:
            x_range = list(range(minx, maxx + 1))

        return x_range

    def to_string(self, sand: Set[Point], with_floor=False) -> str:
        x_range = self.make_x_range(sand, with_floor)
        if with_floor:
            y_range = list(range(0, self.bbox[1].y + 1 + 2))
        else:
            y_range = list(range(0, self.bbox[1].y + 1))

        canvas = [["."] * len(x_range) for _ in y_range]

        for seg in self.segments:
            seg_x = range_it(seg[0].x, seg[1].x)
            seg_y = range_it(seg[0].y, seg[1].y)
            
            for x in seg_x:
                for y in seg_y: 
                    xloc = x - x_range[0]
                    
                    
                    canvas[y][xloc] = "#"

        for pt in sand:
            canvas[pt.y][pt.x - x_range[0]] = "o"

        if with_floor:
            for x in x_range:
                canvas[self.bbox[1].y + 2][x - x_range[0]] = "#"

        return "\n".join(["".join(row) for row in canvas])

    def free_at(self, x: int, y: int, with_floor = False) -> bool:
        if with_floor and y == self.bbox[1].y + 2:
            return False

        for pt1, pt2 in self.segments:
            if between(x, pt1.x, pt2.x) and between(y, pt1.y, pt2.y):
                return False

        return True

    def check_bbox(self, pt: Point) -> bool:
        return not between(pt.x, self.bbox[0].x, self.bbox[1].x) or pt.y > self.bbox[1].y


def read_point(point_str: str):
    x, y = [v.strip() for v in point_str.split(",")]

    return Point(int(x), int(y))

def read_segments(line: str) -> List[Tuple[Point, Point]]:
    points = line.split(" -> ")

    segs = []
    for i in range(len(points) - 1):
        pt1 = read_point(points[i])
        pt2 = read_point(points[i+1])

        segs.append((pt1, pt2))

    return segs

def get_min_max(segments: List[Tuple[Point, Point]]) -> Tuple[Tuple[int, int], Tuple[int, int]]:
    min_x = min(pt.x for seg in segments for pt in seg)
    max_x = max(pt.x for seg in segments for pt in seg)
    min_y = min(pt.y for seg in segments for pt in seg)
    max_y = max(pt.y for seg in segments for pt in seg)

    return (min_x, max_x), (min_y, max_y)
    
    
def read_geometry(fname) -> Geometry:
    lines = Path(fname).read_text().split("\n")

    min_x = min_y = max_x = max_y = None
    geom = Geometry([], None)
    for line in lines:
        segments = read_segments(line)

        (smi_x, sma_x), (smi_y, sma_y) =  get_min_max(segments) 
        min_x = smi_x if min_x is None else min(smi_x, min_x)
        min_y = smi_y if min_y is None else min(smi_y, min_y)
        max_x  = sma_x if max_x is None else max(max_x, sma_x)
        max_y = sma_y if max_y is None else max(max_y, sma_y)
        
        geom.segments.extend(segments)

    geom.bbox = (Point(min_x, min_y), Point(max_x, max_y))

    return geom


def free_at(x: int, y: int, geom: Geometry, sand: Set[Point], with_floor = False) -> bool:
    return not Point(x, y) in sand and geom.free_at(x, y, with_floor=with_floor)

def step_sand(geom: Geometry, sand: Set[Point], sand_pos: Point, with_floor = False) -> bool:
    if free_at(sand_pos.x, sand_pos.y + 1, geom, sand, with_floor=with_floor):
        sand_pos.y += 1
        return True
    elif free_at(sand_pos.x - 1, sand_pos.y + 1, geom, sand, with_floor=with_floor):
        sand_pos.x -= 1
        sand_pos.y += 1
        return True
    elif free_at(sand_pos.x + 1, sand_pos.y + 1, geom, sand, with_floor=with_floor):
        sand_pos.x += 1
        sand_pos.y += 1
        return True
    else:
        return False


def part1(debug=False):
    fname = "test.txt" if debug else "input.txt"

    geom = read_geometry(fname)
    
    exited_bbox = False

    sand = set()
    all_sand = set()
    current = Point(500, 0)
    locs = []
    
    sand_idx = 0
    while not exited_bbox:
        all_sand.add(current)
        prev = current.copy()
        updated = step_sand(geom, sand, current)
        while updated and not exited_bbox:
            locs.append(prev)
            
            if debug:
                print(geom.to_string(all_sand))
            
            prev = current.copy()
            updated = step_sand(geom, sand, current)
            
            exited_bbox = geom.check_bbox(current)
            # input("")
        sand_idx += 1
        if not exited_bbox:
            sand.add(current)
        if debug:
            print(f"Resting place: {current.x, current.y}")
        current = locs[-1]
        locs.pop()
        if debug:
            print(f"Adding sand at {current.x, current.y}")

    if debug:
        print(geom.to_string(sand))
        print(geom.bbox)

    print(f"Grains of sand: {len(sand)}")

import sys
if len(sys.argv) > 1:
    debug = sys.argv[1] == "1"
else:
    debug = False
print(f"{debug=}")
part1(debug)


def part2(debug=False):
    fname = "test.txt" if debug else "input.txt"

    geom = read_geometry(fname)
    
    resting_at_start = False

    sand = set()
    all_sand = set()
    current = Point(500, 0)
    locs = []
    
    prev_use_cnt = 0
    new_cnt = 0
    
    sand_idx = 0
    while not resting_at_start:
        sand_idx += 1
        if sand_idx % 100 == 0:
            print(f"Processed {sand_idx} grains", end="\r")
        
        all_sand.add(current)
        prev = current.copy()
        updated = step_sand(geom, sand, current, with_floor=True)
        
        while updated:
            locs.append(prev)
        
            if debug:
                print(geom.to_string(all_sand, with_floor=True))
        
            prev = current.copy()
        
            updated = step_sand(geom, sand, current, with_floor=True)
            
            # input("")
        resting_at_start = current.x == 500 and current.y == 0
        sand.add(current)

       
        if len(locs) > 0:
            prev_use_cnt += 1
            current = locs.pop()
        else:
            new_cnt += 1
            current = Point(500, 0)
      
    if debug:
        print(geom.to_string(sand, with_floor=True))
        print(geom.bbox)

    print(f"Used prev loc {prev_use_cnt} times")
    print(f"Used new loc {new_cnt} times")
    print(f"Grains of sand: {len(sand)}    ")

part2(debug)