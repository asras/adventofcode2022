from pathlib import Path
from argparse import ArgumentParser

parser = ArgumentParser()
parser.add_argument("-d", action="store_true")

args = parser.parse_args()


def read_cubes(fname):
    txt = Path(fname).read_text().split("\n")

    return [tuple(map(int, line.split(","))) for line in txt if len(line) > 0]


def neighbors(cube):
    x, y, z = cube

    yield x + 1, y, z
    yield x - 1, y, z
    yield x, y + 1, z
    yield x, y - 1, z
    yield x, y, z + 1
    yield x, y, z - 1
    

def solve(debug):
    fname = "test.txt" if debug else "input.txt"

    cubes = set(read_cubes(fname))

    surface_area = 0
    for cube in cubes:
        for neighbor in neighbors(cube):
            if neighbor not in cubes:
                surface_area += 1

    print(f"Total surface area: {surface_area}")


print("Part 1")
solve(args.d)


def get_bbox(cubes):
    x0 = min(cubes, key=lambda t: t[0])[0]
    x1 = max(cubes, key=lambda t: t[0])[0]
    y0 = min(cubes, key=lambda t: t[1])[1]
    y1 = max(cubes, key=lambda t: t[1])[1]
    z0 = min(cubes, key=lambda t: t[2])[2]
    z1 = max(cubes, key=lambda t: t[2])[2]

    return (x0, y0, z0), (x1, y1, z1)
    

def outside(bbox, current):
    (x0, y0, z0), (x1, y1, z1) = bbox

    x, y, z = current

    return x < x0 or y < y0 or z < z0 or x > x1 or y > y1 or z > z1
    
    

def dfs_out(location, bbox, holes, cubes):

    stack = [(location, [])]
    visited = set()

    while len(stack) > 0:
        current, history = stack.pop()
        visited.add(current)

        if current in holes or outside(bbox, current):
            history.append(current)
            return True, history

        new_history = history.copy()
        new_history.append(current)
        for neighbor in neighbors(current):
            if neighbor not in cubes and neighbor not in visited:
                stack.append((neighbor, new_history.copy()))

    return False, visited
            

def solve_part2(debug):
    fname = "test.txt" if debug else "input.txt"

    cubes = set(read_cubes(fname))
    holes = set()
    pockets = set()
    corner1, corner2 = get_bbox(cubes)

    surface_area = 0
    for cube in cubes:
        for neighbor in neighbors(cube):
            if neighbor not in cubes:
                # Only add surface if it is not completely surrounded by lava
                if neighbor not in holes and neighbor not in pockets:
                    # Do DFS to try to get out of BBOX
                    exited, visited_locs = dfs_out(neighbor, (corner1, corner2), holes, cubes)
                    if exited:
                        for loc in visited_locs:
                            holes.add(loc)
                        surface_area += 1
                    else:
                        for loc in visited_locs:
                            pockets.add(loc)
                elif neighbor in holes:
                    surface_area += 1


    print(f"Total surface area: {surface_area}")




print("Part 2")
solve_part2(args.d)





    
