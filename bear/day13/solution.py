from typing import List, Tuple, Optional
from functools import cmp_to_key

def read_packet_pair(pair):
    p1, p2 = pair.split("\n")

    return eval(p1), eval(p2)

def read_packets(fname: str) -> List[Tuple[List[any], List[any]]]:
    with open(fname, "r") as f:
        contents = [l for l in f.read().split("\n\n") if len(l) > 0]

    return [read_packet_pair(pair) for pair in contents]


def in_order(pair) -> Optional[bool]:
    left, right = pair

    for i in range(len(right)):
        if i >= len(left):
            return True

        lval = left[i]
        rval = right[i]

        if type(lval) == int and type(rval) == int:
            if lval < rval:
                return True
            elif lval > rval:
                return False
        elif type(lval) == list and type(rval) == list:
            sublist_in_order = in_order((lval, rval))
            if sublist_in_order is not None:
                return sublist_in_order
        elif type(lval) == int and type(rval) == list:
            sublist_in_order = in_order(([lval], rval))
            if sublist_in_order is not None:
                return sublist_in_order
        elif type(lval) == list and type(rval) == int:
            sublist_in_order = in_order((lval, [rval]))
            if sublist_in_order is not None:
                return sublist_in_order
    
    if len(left) < len(right):
        return True
    elif len(left) > len(right):
        return False
    else:
        return None


def part1(debug=False):
    fname = "test.txt" if debug else "input.txt"

    packets = read_packets(fname)

    indices = []
    for pidx, packet in enumerate(packets):
        val = in_order(packet)
        assert val is not None
        if val:
            indices.append(pidx + 1)
        
    print(f"Oh christmas the sum is {sum(indices)}")

part1()


def part2(debug=False):
    fname = "test.txt" if debug else "input.txt"

    packets = read_packets(fname)
    # Flatten it
    packets = [val for tup in packets for val in tup]

    divider1 = [[2]]
    divider2 = [[6]]
    packets.append(divider1)
    packets.append(divider2)

    def comparer(a, b):
        val = in_order((a, b))
        if val is None:
            return 0
        elif val:
            return -1
        else:
            return 1

    packets.sort(key=cmp_to_key(comparer))
    val = 1
    for idx, el in enumerate(packets):
        if el == divider1 or el == divider2:
            val *= (idx + 1)


    print(f"Oh christmas has come: {val}")

part2()


