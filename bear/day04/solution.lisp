(require :uiop)

(defun read-lines (filename)
  (with-open-file (h filename)
    (loop for line = (read-line h nil nil)
          while line
          collect line)))


(defun parse-range (range)
  (mapcar #'parse-integer (uiop:split-string range :separator "-")))


(defun parse-line (line)
  (let* ((pairs (uiop:split-string line :separator ","))
         (first-range (parse-range (car pairs)))
         (second-range (parse-range (cadr pairs))))
    (list first-range second-range)))


(defun a-contains-b? (range-a range-b)
  (let ((a1 (car range-a))
       (a2 (cadr range-a))
       (b1 (car range-b))
       (b2 (cadr range-b)))
    (and (<= a1 b1) (>= a2 b2))))


(defun one-contains-other? (ranges)
  (let ((range1 (car ranges))
        (range2 (cadr ranges)))
    (or (a-contains-b? range1 range2) (a-contains-b? range2 range1))))


(defun part-1 ()
  (let ((lines (read-lines "input.txt")))
    (count-if #'one-contains-other?
              (mapcar #'parse-line lines))))
  
(format t "The answer to part 1 is: ~a ~%" (part-1))


(defun ranges-overlap? (ranges)
  (let* ((range-a (car ranges))
         (range-b (cadr ranges))
         (a1 (car range-a))
         (a2 (cadr range-a))
         (b1 (car range-b))
         (b2 (cadr range-b)))
    (or
     (and (<= a1 b1) (<= b1 a2))
     (and (<= a1 b2) (<= b2 a2))
     (and (<= b1 a1) (<= a1 b2))
     (and (<= b1 a2) (<= a2 b2)))))


(defun part-2 ()
  (let ((lines (read-lines "input.txt")))
    (count-if #'ranges-overlap?
              (mapcar #'parse-line lines))))

(format t "The answer to part 2 is: ~a ~%" (part-2))
    


