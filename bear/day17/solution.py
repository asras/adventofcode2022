from pathlib import Path
from argparse import ArgumentParser

parser = ArgumentParser()
parser.add_argument("-d", action="store_true")
parser.add_argument("-i", action="store_true")
parser.add_argument("-p", action="store_true")
args = parser.parse_args()

def read_jets(fname):
    return Path(fname).read_text().strip()

def rock1():
    for i in range(4):
        yield (i, 0)

def rock2():
    yield (0, 0)
    yield (2, 0)
    yield (1, -1)
    yield (1, 1)


def rock3():
    for i in range(3):
        yield (i, 0)
    for i in range(2):
        yield (2, i + 1)

def rock4():
    for i in range(4):
        yield (0, i)

def rock5():
    yield (0, 0)
    yield (1, 0)
    yield (0, 1)
    yield (1, 1)

def idx(x, y):
    return x + y * 8

def unidx(idx):
    x = idx % 8
    y = idx // 8
    return x, y

def check_row(row_num, frozen):
    for col in range(1, 8):
        if not idx(col, row_num) in frozen:
            return False

    return True


def drop_rock(time0, jets, frozen, rock_idx, min_y, max_y, interactive):
    x = 3
    y = max_y + 4 + (rock_idx == 1)
    time = time0
    while True:
        # Setup jet
        jet_motion = 0
        match jets[time % (len(jets))]:
            case "<":
                # Left
                jet_motion = -1
                pass
            case ">":
                # Right
                jet_motion = 1
                pass
            case x:
                raise Exception("This aint no jet")

        

        # Calculate sideways collisions
        for ox, oy in rocks[rock_idx]():
            if ox + x + jet_motion <= 0 or ox + x + jet_motion >= 8:
                # Collide with side-wall
                jet_motion = 0
                break
            if idx(x + ox + jet_motion, y + oy) in frozen:
                # Collide with frozen rock
                jet_motion = 0
                break

        # Apply jet
        x += jet_motion

        # Setup grav motion
        grav_motion = -1

        # Calculate downwards collisions
        for ox, oy in rocks[rock_idx]():
            if y + oy + grav_motion <= 0:
                # Collide with floor
                grav_motion = 0
                break
            if idx(x + ox, y + oy + grav_motion) in frozen:
                # Collide with frozen rock
                grav_motion = 0
                break
        
        # Apply gravity
        y += grav_motion

        time += 1

        if interactive:
            # Draw screen
            draw_screen(x, y, min_y, max_y, frozen, rock_idx)
            input("")

        # Handle final collision
        if grav_motion == 0:
            return x, y, time
    

def draw_screen(x, y, min_y, max_y, frozen, rock_idx):
    # Draw screen
    for draw_y in range(max_y, min_y-1, -1):
        for draw_x in range(0, 9):
            if draw_x == 0 or draw_x == 8:
                print("|", end="")
            elif draw_y == 0:
                print("-", end="")
            elif idx(draw_x, draw_y) in frozen:
                print("#", end="")
            else:
                rocked = False
                for ox, oy in rocks[rock_idx]():
                    if x + ox == draw_x and y + oy == draw_y:
                        print("@", end="")
                        rocked = True
                        break
                if not rocked:
                    print(".", end="")
            
        print()
    print()                    
    for i in range(5):
        for j in range(5):
            rocked = False
            for ox, oy in rocks[rock_idx]():
                if ox == j and oy == 3-i:
                    print("@", end="")
                    rocked = True
                    break
            if not rocked:
                print(".", end="")
        print()
    print()


def get_state(max_y, frozen):
    state = []
    for i in range(1, 8):
        y = max_y
        while y > 0:
            if idx(i, y) in frozen:
                break
            y -= 1
        state.append(y)

    state = [y - state[0] for y in state]

    return tuple(state)

rock_heights = [0, 1, 2, 3, 1]

rocks = [rock1, rock2, rock3, rock4, rock5]

def solve(count, debug=False, interactive=False, partially_interactive=False):
    fname = "test.txt" if debug else "input.txt"
    jets = read_jets(fname)
    max_y = 0
    min_y = 0
    extra_y = 0
    jumped = False

    frozen = set()

    cnt = 0
    states =  {}

    time = 0
    i = 0
    while i < count:
        cnt +=  1
        rock_idx = i % 5

        rest_x, rest_y, time = drop_rock(time, jets, frozen, rock_idx, min_y, max_y, interactive)
        # Update max_y
        max_y = max(max_y, rest_y + rock_heights[rock_idx])

        # Freeze rock
        for ox, oy in rocks[rock_idx]():
            frozen.add(idx(rest_x + ox, rest_y + oy))

        if partially_interactive:
            draw_screen(rest_x, rest_y, min_y, max_y, frozen, rock_idx)
            input("")

        state = get_state(max_y, frozen)

        if not jumped and (rock_idx, time % len(jets), state) in states:
            print("Engage jump drive!")
            # We can progress time
            old_i, old_max_y = states[(rock_idx, time % len(jets), state)]
            i_diff = i - old_i
            y_diff = max_y - old_max_y
            # Cant jump all the way to 2022 because of zero indexing
            i_missing = count - 1 - i
            n_cycles = i_missing // i_diff
            extra_y = n_cycles * y_diff
            
            cnt = 0
            jumped = True
            i = i + n_cycles * i_diff
            print(f"Jumped {n_cycles} cycles of length {i_diff}")
        else:
            states[(rock_idx, time % len(jets), state)] = (i, max_y)

        # Continue to next rock
        i = i + 1

    print(f"Tower height: {max_y+extra_y}")


print("Part 1:")
solve(2022, args.d, args.i, args.p)

print("Part 2:")
solve(1000000000000, args.d, args.i, args.p)