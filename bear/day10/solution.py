def signal_strength(CYCLE_COUNTER, REGISTER_X):
    return CYCLE_COUNTER * REGISTER_X


def readlines(fname):
    with open(fname, "r") as f:
        lines = f.read().split("\n")

    return [l for l in lines if len(l) > 0]


def parse_instruction(line):
    line = line.strip()
    parts = line.split(" ")
    match parts:
        case ["noop"]:
            return "noop"
        case ["addx", n]:
            return "addx", int(n)
        case x:
            raise Exception(f"Unknown instruction {x}")
        

def part1(debug=False):
    CYCLE_COUNTER = 0
    REGISTER_X = 1

    fname = "test.txt" if debug else "input.txt"

    insts = readlines(fname)

    strengths = []
    exec_time = None
    for inst in insts:
        parts = inst.split(" ")
        match parts:
            case ["noop"]:
                exec_time = 1
                def op():
                    return 0
            case ["addx", n]:
                exec_time = 2
                def op():
                    return int(n)
            case x:
                raise Exception(f"Unknown instruction {x}")

        for i in range(exec_time):
            CYCLE_COUNTER += 1
            if (CYCLE_COUNTER - 20) % 40 == 0:
                # print(f"Sig strength cycle {CYCLE_COUNTER}: {signal_strength(CYCLE_COUNTER, REGISTER_X)}")
                sig_strength = signal_strength(CYCLE_COUNTER, REGISTER_X)
                strengths.append(sig_strength)
        REGISTER_X += op()

    print(f"Sum sig: {sum(strengths[:6])}")
                

part1()



def sprite_overlaps_cursor(cursor_pos, register_val):
    return abs(cursor_pos - register_val) <= 1
    

def part2(debug=False):
    CYCLE_COUNTER = 0
    REGISTER_X = 1

    fname = "test.txt" if debug else "input.txt"

    insts = readlines(fname)

    # Horizontal position of cursor
    cursor_pos = 0
    screen_width = 40
    screen_height = 6
    image = []
    
    exec_time = None
    for inst in insts:
        parts = inst.split(" ")
        match parts:
            case ["noop"]:
                exec_time = 1
                def op():
                    return 0
            case ["addx", n]:
                exec_time = 2
                def op():
                    return int(n)
            case x:
                raise Exception(f"Unknown instruction {x}")

        for i in range(exec_time):
            if sprite_overlaps_cursor(cursor_pos, REGISTER_X):
                image.append("#")
            else:
                image.append(".")
            
            CYCLE_COUNTER += 1
            cursor_pos = CYCLE_COUNTER % screen_width
            
        REGISTER_X += op()

    for i in range(len(image)):
        print(image[i], end="")
        if (i + 1) % screen_width == 0:
            print()


part2()           
