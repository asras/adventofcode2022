fn main() {
    let contents = std::fs::read_to_string("../../test.txt").unwrap();


    let lines = contents.split("\n").filter(|l| l.len() > 0);

    for line in lines {
        match line.split(" ").collect::<Vec<&str>>().as_slice() {
            ["addx", n] => {
                let int_val = n.parse::<i32>().unwrap();
                println!("Addx: {int_val}");
            }
            ["noop"] => println!("noop"),
            x => panic!("Unknown inst {x:?}"),
        }
    }
    
    // println!("{}", contents);

    
    println!("Hello, world!");
}
