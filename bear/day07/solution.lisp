(load "../lisp/std.lisp")


(defvar dir-stack nil)
(defvar filesystem (make-hash-table))
(defvar id-syms (make-hash-table))


(defmacro gwd ()
  `(gethash (current-folder) filesystem))

(defun current-folder ()
  (make-sym (format nil "~{~a~^#~}" (reverse dir-stack))))

(defun startswith-numbers (line)
  (parse-integer line :junk-allowed t))

(defun process-line (line)
  (cond
    ((startswith "$ cd" line)
     (let ((folder (caddr (split line " "))))
       (if (string-equal ".." folder)
           (pop dir-stack)
           (push (string-trim " " folder) dir-stack))))
    ((startswith "dir" line)
     ;; Insert dir into folder in filesystem
     (let* ((folder (cadr (split line " ")))
            (current-folders (gethash (current-folder) filesystem)))
       (setf (gwd) (cons (list :dir folder) current-folders))))
    ((startswith-numbers line)
     (let ((filesize (parse-integer line :junk-allowed t))
           (filename (string-trim " " (cadr (split line " "))))
           (current-content (gwd)))
       (setf (gwd) (cons (list :file filename filesize) current-content))))
    ((startswith "$ ls" line)
     nil)
    (t
     (format t "Unknown line '~a'~%" line))))


(defun folder-join (folder1 folder2)
  (concatenate 'string folder1 "#" folder2))

(defun calculate-folder-size (folder)
  (let ((contents (gethash (make-sym (format nil "~a" folder)) filesystem)))
    (loop for content in contents
          sum (if (eq (car content) :dir)
                  ;; Recursively go through subfolders
                  (calculate-folder-size (folder-join folder (cadr content)))
                  ;; file with structure (:file filename filesize)
                  (caddr content)))))


(defun reset-filesystem ()
  (setf dir-stack nil)
  (setf filesystem (make-hash-table)))

(defun part-1 ()
  (reset-filesystem)
  (let ((input (read-lines "input.txt")))
    (loop for line in input
          do (process-line line))
    (loop for k being the hash-keys of filesystem
          sum
             (let ((folder-size (calculate-folder-size (format nil "~a" k))))
               (if (<= folder-size 100000)
                   folder-size
                   0)))))


(format t "Now this is Christmas: ~a~%" (part-1))


(defun min-by (fun list)
  (let ((min-obj nil)
        (min-val nil))
    (loop for thing in list
          do
             (let ((value (funcall fun thing)))
               (if (or (not min-obj) (< value min-val))
                   (block what
                       (setf min-obj thing)
                       (setf min-val (funcall fun thing))))))
    min-obj))


(defun part-2 ()
  (reset-filesystem)
  (let ((input (read-lines "input.txt")))
    (loop for line in input do (process-line line))
    (let* ((total-size (calculate-folder-size "/"))
           (free (- 70000000 total-size))
           (needed-free (- 30000000 free)))
      (let ((candidates nil))
        (loop for folder being the hash-keys of filesystem
              do (let ((folder-size (calculate-folder-size (format nil "~a" folder))))
                   (if (>= folder-size needed-free)
                       (push (list folder folder-size) candidates))))
        (cadr (min-by (lambda (cand) (cadr cand)) candidates))))))


(format t "All I want for Christmas is ~a bytes~%" (part-2))

      
        
        
