from typing import List, Tuple


def read_input(fname):
    with open(fname, "r") as f:
        txt = f.read()

    lines = [int(x) for x in txt.split("\n") if len(x) > 0]

    return lines


def move_number_at(idx: int, values: List[Tuple[int, int]], sequence: List[int]) -> None:
    value, my_id, _ = values[idx]
    times = abs(value)
    forward = value >= 0

    for time_idx in range(times):
        crossed_right = False
        crossed_left = False
        new_idx = idx + (1 if forward else -1)
        if new_idx == len(values):
            new_idx = 0
            crossed_right = True
        if new_idx == -1:
            new_idx = len(values) - 1
            crossed_left = True

        swapped_id = values[new_idx][1]

        values[idx], values[new_idx] = values[new_idx], values[idx]
        sequence[my_id] = new_idx
        sequence[swapped_id] = idx
        if crossed_right:
            # Move rightmost element all the way to the left
            for i in range(len(values) - 1, 0, -1):
                left_id = values[i - 1][1]
                right_id = values[i][1]
                values[i], values[i - 1] = values[i - 1], values[i]
                sequence[left_id] = i
                sequence[right_id] = i - 1
            new_idx += 1
        elif crossed_left:
            # Move leftmost element all the way to the right
            for i in range(len(values) - 1):
                left_id = values[i][1]
                right_id = values[i + 1][1]
                values[i], values[i + 1] = values[i + 1], values[i]
                sequence[left_id] = i + 1
                sequence[right_id] = i
            new_idx -= 1

        idx = new_idx


def get_after(value, values, sequence, steps):
    id = value[1]
    index = sequence[id]
    value = values[(index + steps) % len(input_numbers)]
    return value[2]


def mmod(a, b):
    return (a + b) % b - (b if a < 0 else 0)


if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument("-t", action="store_true", dest="test", default=False)
    parser.add_argument("-s", action="store_true", dest="sanity_check", default=False)
    parser.add_argument("-p", action="store_true", dest="part2", default=False)

    args = parser.parse_args()

    if args.test:
        input_numbers = read_input("test.txt")
    else:
        input_numbers = read_input("input.txt")

    if args.part2:
        decrypt_key = 811589153
        input_numbers = [(n * decrypt_key) for n in input_numbers]

    values = []
    zero_value = None
    for idx, number in enumerate(input_numbers):
        values.append((mmod(number, len(input_numbers) - 1), idx, number))
        if number == 0:
            zero_value = (mmod(number, len(input_numbers) - 1), idx, number)

    sequence = list(range(len(input_numbers)))

    print(f"Sequence length: {len(sequence)}")

    factor = 10 if args.part2 else 1
    for i in range(len(sequence) * factor):
        print(f"Processing {i+1}/{len(sequence) * factor}   ", flush=True, end="\r")
        move_number_at(sequence[i % len(sequence)], values, sequence)
        if args.sanity_check:
            for id, index in enumerate(sequence):
                value_at_index, id_for_value = values[index]
                assert id == id_for_value

    n1 = get_after(zero_value, values, sequence, 1000)
    n2 = get_after(zero_value, values, sequence, 2000)
    n3 = get_after(zero_value, values, sequence, 3000)
    print(f"{n1=}, {n2=}, {n3=}")
    print(f"Result: {n1+n2+n3}")
