

def read_input(fname):
    with open(fname, "r") as f:
        txt = f.read()

    lines = [int(x) for x in txt.split("\n") if len(x) > 0]
    # lines = [(x, i-1, i+1, 0) for i, x in enumerate(lines)]
    # zero_idx = next(idx for idx, val in enumerate(lines) if val[0] == 0)
    zero_idx = next(idx for idx, val in enumerate(lines) if val == 0)

    return lines, zero_idx, len(lines)


def _update_pos(positions, index):
    val, prev, nex, offset = positions[index]
    new_index = (val + offset) % len(positions)

    p_val, p_prev, p_nex, p_offset = positions[prev]
    n_val, n_prev, n_nex, n_offset = positions[nex]

    # Connect old previous and old next
    positions[prev] = (p_val, p_prev, nex, p_offset + 1)
    positions[nex] = (n_val, prev, n_nex, n_offset - 1)

    # Connect self to new previous and new next
    new_p_val, new_p_prev, new_p_nex, new_p_offset = positions[new_index]
    positions[index] = (val, new_index, new_p_nex, offset)
    positions[new_index] = (new_p_val, new_p_prev, index, new_p_offset + 1)

    v1, pr1, ne1, of1 = positions[new_p_nex]
    positions[new_p_nex] = (v1, index, ne1, of1 - 1)


def update_pos(positions, index):
    prev_list = positions[:index]
    after_list = positions[index+1:]

    new_index = (index + positions[index]) % len(positions)
    if positions[index] < 0:
        new_index -= 1
    print(f"Updating {positions[index]} to {new_index}")

    if new_index == index:
        return positions
    elif new_index > index:
        # Everything in between gets shuffled
        between = positions[index + 1:new_index + 1]
        return prev_list + between + [positions[index]] + positions[new_index + 1:]
    else:  # index > new_index
        between = positions[new_index:index]
        return positions[:new_index] + [positions[index]] + between + after_list


def _print_in_order(positions):
    current_idx = 0
    for i in range(len(positions)):
        current_val, _, current_idx, _ = positions[current_idx]
        print(current_val, end=", ")
    print("")


def print_in_order(positions):
    print(", ".join(str(v) for v in positions))


def main(debug):
    fname = "test.txt" if debug else "input.txt"

    positions, zero_idx, count = read_input(fname)

    print_in_order(positions)
    positions = update_pos(positions, 0)
    print_in_order(positions)
    positions = update_pos(positions, 0)
    print_in_order(positions)
    positions = update_pos(positions, 1)
    print_in_order(positions)
    positions = update_pos(positions, 2)
    print_in_order(positions)


main(debug=True)
