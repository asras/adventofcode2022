module Main (main) where

import PuzzleOne
import Data1
import PuzzleTwo
import PuzzleSix

main :: IO ()
main = do
         one
         task
         six

