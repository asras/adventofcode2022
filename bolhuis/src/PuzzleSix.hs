module PuzzleSix where

import Data.List
import Data.List.Utils

six :: IO ()
six = do
  content <- readFile "resources/day6.txt"

  let result = finder content checkerOne
  let resultTwo = finder content checkerTwo

  print "Day 6 ---"
  print content

  print result
  print $ length result + 4 -- add 4 due to look-ahead size of 4

  print resultTwo
  print $ length resultTwo + 14 -- add 14 due to look-ahead size of 14

validSignal :: String -> Bool
validSignal str =
    length str == 4 && (nub str == str)

validMessage :: String -> Bool
validMessage str =
    length str == 14 && (nub str == str)

finder :: [a] -> ([a] -> Bool) -> [a]
finder str checker =
  takeWhileList checker str

checkerOne :: [Char] -> Bool
checkerOne str =
  not $ validSignal (take 4 str)

checkerTwo :: [Char] -> Bool
checkerTwo str =
  not $ validMessage (take 14 str)