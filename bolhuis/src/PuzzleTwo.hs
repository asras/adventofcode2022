module PuzzleTwo
  ( task )
where

task :: IO ()
task = do
  contents <- readFile "resources/day2.txt"
  let myLines = lines contents
  let rounds = map parseRound myLines
  let sumScore = sum rounds
  let sumTacticalScore = sum (map parseTacticalRound myLines)

  print sumScore
  print sumTacticalScore


data RPS = Rock | Paper | Scissors deriving (Show, Eq)
data Result = Win | Lose | Draw deriving (Show, Eq)
type Round = (Result, Int)

parseRPS :: String -> RPS
parseRPS input
  | input == "A" = Rock
  | input == "X" = Rock
  | input == "B" = Paper
  | input == "Y" = Paper
  | input == "C" = Scissors
  | input == "Z" = Scissors
  | otherwise = error "Invalid input"

parseTarget :: String -> Result
parseTarget x
  | x == "X" = Lose
  | x == "Y" = Draw
  | x == "Z" = Win
  | otherwise = error "Invalid input"

playRound :: RPS -> RPS -> Result
playRound opponent player
  | opponent == Rock && player == Scissors = Lose
  | opponent == Rock && player == Paper = Win
  | opponent == Rock && player == Rock = Draw
  | opponent == Paper && player == Rock = Lose
  | opponent == Paper && player == Scissors = Win
  | opponent == Paper && player == Paper = Draw
  | opponent == Scissors && player == Paper = Lose
  | opponent == Scissors && player == Rock = Win
  | opponent == Scissors && player == Scissors = Draw
  | otherwise = error "Invalid input"

calculatePlay :: RPS -> Result -> RPS
calculatePlay hand res
  | res == Draw = hand
  | hand == Rock && res == Win = Paper
  | hand == Rock && res == Lose = Scissors
  | hand == Paper && res == Win = Scissors
  | hand == Paper && res == Lose = Rock
  | hand == Scissors && res == Win = Rock
  | hand == Scissors && res == Lose = Paper


parseRound :: String -> Int
parseRound input =
  scoreRound player result
  where
    hands = words input
    opponent = parseRPS (hands !! 0)
    player = parseRPS (hands !! 1)
    result = playRound opponent player

scoreRound :: RPS -> Result -> Int
scoreRound player result =
  getRpsValue player + getRoundValue result

parseTacticalRound :: String -> Int
parseTacticalRound str =
  scoreRound player target
  where
    hands = words str
    opponent = parseRPS (hands !! 0)
    target = parseTarget (hands !! 1)
    player = calculatePlay opponent target

getRpsValue :: RPS -> Int
getRpsValue Rock = 1
getRpsValue Paper = 2
getRpsValue Scissors = 3

getRoundValue :: Result -> Int
getRoundValue Win = 6
getRoundValue Draw = 3
getRoundValue Lose = 0
