module PuzzleOne
  ( puzzles,
    one
  )
where

import Data.List
import Data.List.Split
import Data1

one :: IO ()
one =
  let a = splitOn "\n" dataOne
      b = splitOn [""] a
      elves = map (map (read :: String -> Int)) b
      sums = map sum elves
      top = take 3 (reverse (sort sums))
   in do
        print (maximum sums)
        print (sum top)


puzzles :: [IO ()]
puzzles = [one]
