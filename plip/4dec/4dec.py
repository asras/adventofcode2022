# Last Christmas I gave you my Morten,
# but the very next day,
# Asbjorn took me away

# 1
with open('input.txt') as f:

    assign_sum = 0 
    
    for line in f:

        line_s = line.strip().split(',')

        # Convert elf1 to range of numbers
        elf1_ran1 = line_s[0].split('-')[0]
        elf1_ran2 = line_s[0].split('-')[1]
        ran1 = range(int(elf1_ran1), int(elf1_ran2)+1)

        # Convert elf2 to range of numbers
        elf2_ran1 = line_s[1].split('-')[0]
        elf2_ran2 = line_s[1].split('-')[1]
        ran2 = range(int(elf2_ran1), int(elf2_ran2)+1)

        # Which numbers overlap
        same_assign = list(set(ran1) & set(ran2))

#         # Check in both directions
#         elf1_in_elf2 = all(e in same_assign for e in ran2)
#         elf2_in_elf1 = all(e in same_assign for e in ran1)

#         if elf1_in_elf2 or elf2_in_elf1:
#             assign_sum += 1

# print(assign_sum)

# 2
        # Check in both directions
        elf1_in_elf2 = any(e in same_assign for e in ran2)
        elf2_in_elf1 = any(e in same_assign for e in ran1)
        
        if elf1_in_elf2 or elf2_in_elf1:
            assign_sum += 1

print(assign_sum)
