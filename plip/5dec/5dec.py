# Its beginning to look at lot like coding

# 1
with open('test_input.txt') as f:

    cargo_stack = []
    
    for line in f:
        # break at row with col nr info
        if all([nr.isdigit() for nr in line.split()]):
            col_nr = int(max(line.split()))
            break

        # store cargo in list cargo_stack
        window_size = 4
        for l in range(0, len(line)-window_size+1, window_size):
            cargo = line[l:l+window_size].strip()
            cargo_stack.append(cargo)

    # create a list "array" and transpose
    cargo_stack_arr = [cargo_stack[c:c+col_nr] for c in range(0, len(cargo_stack), col_nr)]
    cargo_stack_arr_t = [list(c) for c in zip(*cargo_stack_arr)]

    # remove empty elements
    for col in cargo_stack_arr_t:
        while '' in col:
            col.remove('')

    # skip empty line
    next(f)  

    # rest of inout are commands for stacking cargo
    commands = f.readlines()

# # do commands
# for command in commands:
#     command_s = command.strip().split()
    
#     cargo_amount = int(command_s[1])
#     cargo_source = int(command_s[3])
#     cargo_dest = int(command_s[5])
    
#     for n in range(cargo_amount):  # n. number of cargo
#         top_cargo = cargo_stack_arr_t[cargo_source-1][0]
#         cargo_stack_arr_t[cargo_dest-1].insert(0,top_cargo)
#         cargo_stack_arr_t[cargo_source-1].pop(0)
        
# result = ''
# for col in cargo_stack_arr_t:
#     result += col[0]

# print(result)

# 2
# do commands
for command in commands:

    print(cargo_stack_arr_t)

    command_s = command.strip().split()
    print(command_s)
    cargo_amount = int(command_s[1])
    cargo_source = int(command_s[3])
    cargo_dest = int(command_s[5])
    
    for n in range(cargo_amount):  # n. number of cargo
        top_cargo = cargo_stack_arr_t[cargo_source-1][cargo_amount-1-n] # now inverse order
        cargo_stack_arr_t[cargo_dest-1].insert(0,top_cargo)

    for n in range(cargo_amount): # now pop only after all cargo crates have been moved
        cargo_stack_arr_t[cargo_source-1].pop(0)
    
result = ''
for col in cargo_stack_arr_t:
    result += col[0]

print(result)
