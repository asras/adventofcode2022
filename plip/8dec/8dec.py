# Asbjorn is a nerdy elf

import numpy as np

# # 1
with open('input.txt') as f:

    arr = np.array([list(line.strip()) for line in f], int)

print(arr)

row_nr, col_nr = np.shape(arr)

# # Number of trees on edge
# edge_visible = 2*row_nr + 2*(col_nr-2)

# # Loop through inner trees only
# inner_visible = 0

# for t in range(1, row_nr-1):
#     for tt in range(1, col_nr-1):
        
#         tree = arr[t, tt]
#         trees_top = arr[:t,tt]
#         trees_bot = arr[t+1:,tt]
#         trees_right = arr[t,tt+1:]
#         trees_left= arr[t,:tt:]        

#         top_line_max = max(trees_top)
#         bot_line_max = max(trees_bot)
#         right_line_max = max(trees_right)
#         left_line_max = max(trees_left)       

#         if tree > top_line_max:
#             inner_visible += 1
#         elif tree > bot_line_max:
#             inner_visible += 1
#         elif tree > right_line_max:
#             inner_visible += 1
#         elif tree > left_line_max:
#             inner_visible += 1

# total_visible = inner_visible + edge_visible
# print(total_visible)

# 2
# Loop through all trees now
scenic_score_new = 0

for t in range(row_nr):
    for tt in range(col_nr):
        
        tree = arr[t, tt]
        trees_top = arr[:t,tt]
        trees_bot = arr[t+1:,tt]
        trees_right = arr[t,tt+1:]
        trees_left= arr[t,:tt:]

        # Dist going top
        dist_top = 0
        for tree_top in reversed(trees_top):            
            if tree <= tree_top:
                dist_top += 1
                break
            dist_top += 1

        # Dist going bottom
        dist_bot = 0
        for tree_bot in trees_bot:            
            if tree <= tree_bot:
                dist_bot += 1
                break
            dist_bot += 1

        # Dist going left
        dist_left = 0
        for tree_left in reversed(trees_left):            
            if tree <= tree_left:
                dist_left += 1
                break
            dist_left += 1

        # Dist going right
        dist_right = 0
        for tree_right in trees_right:
            if tree <= tree_right:
                dist_right += 1
                break
            dist_right += 1

        # Compute total score by multiplication
        scenic_score = dist_top*dist_bot*dist_left*dist_right

        # Find max
        if scenic_score > scenic_score_new:
            scenic_score_new = scenic_score

print(scenic_score_new)

           
