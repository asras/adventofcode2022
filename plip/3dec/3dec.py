# And so this is Asbjoern
import string
from itertools import islice

# Create one dict of priorities including both lower- and uppercase
lowercase = [*string.ascii_lowercase]
uppercase = [*string.ascii_uppercase]
merged = lowercase + uppercase

priorities = dict(zip(merged, range(1, 53)))

# 1
with open('input.txt') as f:

#     priority_sum = 0
    
#     for line in f:        
#         line_s = line.strip()
        
#         if not len(line_s) == len(set(line_s)):
            
#             kombardo_1 = line_s[:len(line_s)//2]
#             kombardo_2 = line_s[len(line_s)//2:]
#             same_item =  set(kombardo_1) & set(kombardo_2)

#             priority = priorities[next(iter(same_item))] #  only 1 item
#             priority_sum += priority
            
# print(priority_sum)

# 2
    priority_sum = 0
    
    while True:
        
        lines = list(islice(f, 3))
        
        if not lines:  # EOF
            break

        same_item =  set(lines[0].strip()) & set(lines[1].strip())\
            & set(lines[2].strip()) # Find the badge

        priority = priorities[next(iter(same_item))]
        priority_sum += priority
            
print(priority_sum)
            
