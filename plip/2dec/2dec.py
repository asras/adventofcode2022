# I am driving home for Morten

# Column 1 - Opponent
# A > Rock
# B > Paper
# C > Scissor

# Column 2 - Elf pliplop
# X > Rock
# Y > Paper
# Z > Scissor

rules = {
    'A X': 3,
    'A Y': 6,
    'A Z': 0,
    'B X': 0,
    'B Y': 3,
    'B Z': 6,
    'C X': 6,
    'C Y': 0,
    'C Z': 3
}

rules_2 = {
    'A X': 'C',
    'A Y': 'A',
    'A Z': 'B',
    'B X': 'A',
    'B Y': 'B',
    'B Z': 'C',
    'C X': 'B',
    'C Y': 'C',
    'C Z': 'A'
}
    
shape_points = {
    'X': 1,
    'Y': 2,
    'Z': 3
}

shape_points_2 = {
    'A': 1,
    'B': 2,
    'C': 3
}


rules_points_2 = {
    'X': 0,
    'Y': 3,
    'Z': 6
}


# 1
with open('input.txt') as f:
#     score = 0
    
#     for line in f:
#         shape = line.strip().split()[1]        
#         shape_outcome = shape_points[shape]  # Outcome from own shape 
#         rules_outcome = rules[line.strip()]  # Outcome from results
#         # print(shape_outcome)
#         # print(rules_outcome)
        
#         score += shape_outcome + rules_outcome

# print(score)
        
# 2
    score = 0

    for line in f:
        shape_outcome = shape_points_2[rules_2[line.strip()]]  # Outcome from own shape
        rules_outcome = rules_points_2[line.strip().split()[1]]

        score += rules_outcome + shape_outcome 

print(score)
        

    
