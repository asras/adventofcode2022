# And so this is Netcompany

# 1 + 2
with open('input.txt') as f:
    
    device_input = list(f.readlines()[0])

    # window_size = 4  # Puzzle #1
    window_size = 14  # Puzzle #2

    for e in range(len(device_input) - window_size + 1):
        window = device_input[e:e+window_size]

        if len(set(window)) == window_size:
            marker = e + window_size  # marker of start-of-pack/messages
            break

print(marker)
