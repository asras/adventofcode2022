use std::{fs, collections::HashSet};

pub fn main() {
    println!("{}", std::env::current_dir().unwrap().display());
    let input = fs::read_to_string("resources/03/input").expect("Kunne ikke åbne input");

    let alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    let rucksacks: Vec<&str> = input.split("\n").filter(|s| s.len() > 0).collect();

    pt1(rucksacks.clone(), alphabet);
    pt2(rucksacks.clone(), alphabet);
}

fn pt1(rucksacks: Vec<&str>, alphabet: &str) {
    let mut total_priority = 0;
    for rucksack in rucksacks {
        let total_items = rucksack.len();
        let comp1 = &rucksack[..total_items/2];
        let comp2 = &rucksack[total_items/2..];

        // println!("rucksack {rucksack}\ncompartment 1 {comp1}\ncompartment 2 {comp2}");
        for char in comp1.chars() {
            if comp2.contains(char) {

                let optional_position = alphabet.find(char);

                if optional_position.is_none() {
                    continue;
                }

                let current_priority = optional_position.unwrap();

                println!("Mixed up item was {char}. Priority is {current_priority}");

                total_priority += current_priority + 1;

                break;
            }
        }
    }

    println!("Total priority found was {total_priority}");
}

fn pt2(rucksacks: Vec<&str>, alphabet: &str) {
    let mut idx = 0;
    let mut badge_priority = 0;
    loop {
        // assign group
        let elf1: HashSet<char> = HashSet::from_iter(rucksacks[idx].chars());
        let elf2: HashSet<char> = HashSet::from_iter(rucksacks[idx+1].chars());
        let elf3: HashSet<char> = HashSet::from_iter(rucksacks[idx+2].chars());
    
        let intersect12: HashSet<char> = elf1.intersection(&elf2).cloned().collect();
        let mut intersect123 = intersect12.intersection(&elf3);

        println!("Intersection {:?}", intersect123);

        let val = intersect123.next().unwrap();
        badge_priority += get_priority(alphabet, val);

        idx += 3;
        if idx >= rucksacks.len() {
            break;
        }
    }

    println!("Badge priority is {:?}", badge_priority);
}

fn get_priority(alphabet: &str, bagde: &char) -> usize {
    return alphabet.find(*bagde).unwrap() + 1;
}

#[cfg(test)]
mod tests {
  use super::*;
  #[test]
  fn test_this() {
    assert!(false);
  }
}