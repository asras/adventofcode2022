use std::collections::{HashMap, VecDeque};
use std::fs;

use regex::Regex;

#[derive(Debug)]
struct Instruction {
    quantity: i32,
    from: i32,
    to: i32,
}

pub fn main() {
    let input = fs::read_to_string("resources/05/input").expect("Kunne ikke åbne input");

    let assignments: Vec<&str> = input.split("\n\n").filter(|s| !s.is_empty()).collect();

    let starting_point = parse_starting_point(assignments[0]);
    let movements = parse_movements(assignments[1]);

    pt1(starting_point.clone(), &movements);
    pt2(starting_point, &movements);
}

fn pt1(starting_point: HashMap<i32, Vec<String>>, movements: &VecDeque<Instruction>) {
    let mut crates = starting_point;
    for movement in movements {
        let mut temp = VecDeque::new();
        {
            let from = &mut crates.get_mut(&movement.from).unwrap();
            for _i in 0..movement.quantity {
                temp.push_back(from.pop().unwrap());
            }
        }

        let to = &mut crates.get_mut(&movement.to).unwrap();
        for _i in 0..movement.quantity {
            let item = temp.pop_front().unwrap();
            to.push(item);
        }
    }

    let mut top_crates: String = "".to_owned();
    for i in 1..=crates.len() {
        let this_crate = crates.get(&i.try_into().unwrap()).unwrap();
        let top_crate = this_crate.last().unwrap();
        top_crates.push_str(&top_crate[1..2]);
    }

    println!("Top crates pt1: {:?}", top_crates);
}

fn pt2(starting_point: HashMap<i32, Vec<String>>, movements: &VecDeque<Instruction>) {
    let mut crates = starting_point;
    for movement in movements {
        let mut temp = vec![];

        {
            let from = &mut crates.get_mut(&movement.from).unwrap();
            for _i in 0..movement.quantity {
                temp.push(from.pop().unwrap());
            }
        }

        let to = &mut crates.get_mut(&movement.to).unwrap();
        for _i in 0..movement.quantity {
            let item = temp.pop().unwrap();
            to.push(item);
        }
    }

    let mut top_crates: String = "".to_owned();
    for i in 1..=crates.len() {
        let this_crate = crates.get(&i.try_into().unwrap()).unwrap();
        let top_crate = this_crate.last().unwrap();
        top_crates.push_str(&top_crate[1..2]);
    }

    println!("Top crates pt2: {:?}", top_crates);
}

fn parse_movements(input_str: &str) -> VecDeque<Instruction> {
    let mut movements = VecDeque::new();

    let rows: Vec<&str> = input_str.split('\n').filter(|s| !s.is_empty()).collect();

    // format 'move 1 from 2 to 1'
    let re = Regex::new(r"^move (?P<quantity>\d+) from (?P<from>\d) to (?P<to>\d)$").unwrap();
    for row in rows {
        let matches = re.captures(row).unwrap();

        movements.push_back(Instruction {
            quantity: matches["quantity"].parse().unwrap(),
            from: matches["from"].parse().unwrap(),
            to: matches["to"].parse().unwrap(),
        });
    }

    movements
}

fn parse_starting_point(input_str: &str) -> HashMap<i32, Vec<String>> {
    let mut queues: HashMap<i32, Vec<String>> = HashMap::new();

    let mut rows: Vec<&str> = input_str.split('\n').filter(|s| !s.is_empty()).collect();

    let columns: Vec<i32> = rows
        .pop()
        .unwrap()
        .split(' ')
        .filter(|s| !s.is_empty())
        .map(|x| x.parse::<i32>().unwrap())
        .collect();

    for column in columns {
        queues.insert(column, vec![]);
    }

    rows.reverse();

    for row in rows {
        let mut queue_number = 1;
        let mut idx = 0;
        loop {
            let current_crate = &row[idx..idx + 3];

            if current_crate.starts_with('[') {
                let queue = &mut queues.get_mut(&queue_number).unwrap();
                queue.push(current_crate.to_string());
            }

            queue_number += 1;
            idx += 4;
            if idx >= row.len() {
                break;
            }
        }
    }

    queues
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_this() {
        assert!(false);
    }
}
