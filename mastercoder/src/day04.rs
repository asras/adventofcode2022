use std::fs;

pub fn main() {   
    let input = fs::read_to_string("resources/04/input").expect("Kunne ikke åbne input");

    let assignments: Vec<&str> = input.split("\n").filter(|s| s.len() > 0).collect();

    pt1(&assignments);
    pt2(&assignments);
}

fn pt1(assignments: &Vec<&str>) {
    let mut lazy = 0;
    for assignment in assignments {
        let sections: Vec<Vec<i32>> = assignment.split(",").map(|x| to_range(x)).collect();

        let elf1 = &sections[0];
        let elf2 = &sections[1];

        let one_in_two = elf1.iter().all(|x| elf2.contains(x));
        let two_in_one = elf2.iter().all(|x| elf1.contains(x));

        if (one_in_two && two_in_one) || (one_in_two && !two_in_one) || (!one_in_two && two_in_one) {
            println!("One lazy elf found! ");
            lazy += 1;
        }
    }

    println!("Total number of lazies with complete overlap: {:?}", lazy);
}

fn pt2(assignments: &Vec<&str>) {
    let mut lazy = 0;
    for assignment in assignments {
        let sections: Vec<Vec<i32>> = assignment.split(",").map(|x| to_range(x)).collect();

        let elf1 = &sections[0];
        let elf2 = &sections[1];

        let one_in_two = elf1.iter().any(|x| elf2.contains(x));
        let two_in_one = elf2.iter().any(|x| elf1.contains(x));

        if (one_in_two && two_in_one) || (one_in_two && !two_in_one) || (!one_in_two && two_in_one) {
            println!("One lazy elf found! ");
            lazy += 1;
        }
    }

    println!("Total number of lazies with any overlap: {:?}", lazy);
}

fn to_range(range_str: &str) -> Vec<i32> {
    let lower_upper: Vec<i32> = range_str.split("-").map(|c| c.parse::<i32>().expect("arhsdjlfh")).collect();

    return (lower_upper[0]..lower_upper[1]+1).collect();
}

#[cfg(test)]
mod tests {
  use super::*;
  #[test]
  fn test_this() {
    assert!(false);
  }
}