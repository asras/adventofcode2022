use std::{collections::HashMap, fs};

use regex::Regex;

#[derive(Debug, PartialEq)]
struct File {
    name: String,
    size: Option<i32>,
}

pub fn main() {
    let input_str = fs::read_to_string("resources/07/input").expect("Kunne ikke åbne input");
    let input: Vec<&str> = input_str.split('\n').filter(|s| !s.is_empty()).collect();

    let filesystem = parse_input(&input);

    pt1(&filesystem);
    pt2(&filesystem);
}

fn pt1(filesystem: &HashMap<String, Vec<File>>) {
    println!("Part the 1");

    let mut total_size = 0;
    for key in filesystem.keys() {
        let dir = filesystem.get(key).unwrap();
        let dir_size = recursive(filesystem, dir);
        if dir_size < 100_000 {
            total_size += dir_size;
        }
    }

    println!("Total size pt1: {:?}", total_size)
}

fn recursive(filesystem: &HashMap<String, Vec<File>>, files_in_dir: &Vec<File>) -> i32 {
    let mut size = 0;
    for file in files_in_dir {
        if is_dir(file) {
            size += recursive(filesystem, filesystem.get(&file.name).unwrap());
        } else {
            size += file.size.unwrap();
        }
    }

    size
}

fn is_dir(file: &File) -> bool {
    file.size.is_none()
}

fn pt2(filesystem: &HashMap<String, Vec<File>>) {
    let mut folder_sizes: HashMap<&String, i32> = HashMap::new();
    for key in filesystem.keys() {
        let dir = filesystem.get(key).unwrap();
        folder_sizes.insert(key, recursive(filesystem, dir));
    }

    let total_filesystem_size = 70_000_000;
    let minimum_size_update = 30_000_000;

    let root_size = folder_sizes.get(&String::from("/")).unwrap();
    let amount_to_free = minimum_size_update - (total_filesystem_size - root_size);

    let mut minimum = total_filesystem_size;
    for size in folder_sizes.values() {
        if size > &amount_to_free && size < &minimum {
            minimum = size.to_owned();
        }
    }

    println!(
        "Minimum size to remove to make room for update: {:?}",
        minimum
    );
}

fn parse_input(input: &Vec<&str>) -> HashMap<String, Vec<File>> {
    let mut filesystem: HashMap<String, Vec<File>> = HashMap::new();

    let change_dir = "$ cd ";
    let list_dir = "$ ls";
    let is_dir = "dir ";
    let move_up_dir = "..";

    let mut n = 0;
    let mut current_working_dir: &str = "/";
    let mut working_directory: Vec<&str> = vec![current_working_dir];
    let file_regex = Regex::new(r"^(?P<size>\d+) (?P<filename>.*)$").unwrap();
    while n < input.len() {
        let operation = input[n];

        if operation.starts_with(list_dir) {
            // continue;
        } else if operation.starts_with(change_dir) {
            current_working_dir = operation.strip_prefix(change_dir).unwrap();

            if current_working_dir == move_up_dir {
                working_directory.pop().unwrap();
            } else {
                working_directory.push(current_working_dir);
            }

            let full_path = get_full_path(&working_directory);

            filesystem
                .entry(full_path)
                .or_insert_with(std::vec::Vec::new);
        } else if operation.starts_with(is_dir) {
            let full_path = get_full_path(&working_directory);
            let dir = filesystem.get_mut(&full_path).unwrap();

            let file = File {
                name: full_path + "/" + operation.strip_prefix(is_dir).unwrap(),
                size: None,
            };

            if !dir.contains(&file) {
                dir.push(file);
            }
        } else {
            let matches = file_regex.captures(operation).unwrap();

            let full_path = get_full_path(&working_directory);
            let dir = filesystem.get_mut(&full_path).unwrap();

            let file = File {
                name: full_path + "/" + &matches["filename"],
                size: Some(matches["size"].parse::<i32>().unwrap()),
            };

            if !dir.contains(&file) {
                dir.push(file);
            }
        }

        n += 1;
    }

    filesystem
}

fn get_full_path(working_dir: &Vec<&str>) -> String {
    let mut path: String = "".to_owned();
    for dir in working_dir {
        path.push_str(dir);

        if dir != &"/" {
            path.push('/');
        }
    }

    if path.len() > 1 {
        path.pop();
    }

    path
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_this() {
        assert!(false);
    }
}
