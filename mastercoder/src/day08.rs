use std::{collections::HashMap, fs};

pub fn main() {
    let input_str = fs::read_to_string("resources/08/input").expect("Kunne ikke åbne input");
    let input: Vec<&str> = input_str.split('\n').filter(|s| !s.is_empty()).collect();

    let mut forrest: HashMap<(usize, usize), i32> = HashMap::new();
    let forrest_size: (usize, usize) = parse_input(input, &mut forrest);

    pt1(&forrest, forrest_size);
    pt2(&forrest, forrest_size);
}

fn pt1(forrest: &HashMap<(usize, usize), i32>, dimensions: (usize, usize)) {
    let mut visible = 0;
    for i in 0..dimensions.0 {
        for j in 0..dimensions.1 {
            let tree = forrest.get(&(i, j)).unwrap();

            visible += is_visible_from_edge(forrest, tree, (i, j), dimensions);
        }
    }

    println!("Trees visible pt1 {:?}", visible);
}

fn is_visible_from_edge(
    forrest: &HashMap<(usize, usize), i32>,
    tree: &i32,
    coordinates: (usize, usize),
    dimensions: (usize, usize),
) -> i32 {
    if is_on_edge(coordinates, dimensions) {
        return 1;
    }

    let mut is_visible = true;
    for i in 0..coordinates.0 {
        let coords = &(i, coordinates.1);
        let current = forrest.get(coords).unwrap();

        if current >= tree {
            is_visible = false;
            break;
        }
    }

    if is_visible {
        return 1;
    }

    is_visible = true;
    for i in coordinates.0 + 1..dimensions.0 {
        let coords = &(i, coordinates.1);
        let current = forrest.get(coords).unwrap();

        if current >= tree {
            is_visible = false;
            break;
        }
    }

    if is_visible {
        return 1;
    }

    is_visible = true;
    for i in 0..coordinates.1 {
        let coords = &(coordinates.0, i);
        let current = forrest.get(coords).unwrap();

        if current >= tree {
            is_visible = false;
            break;
        }
    }

    if is_visible {
        return 1;
    }

    is_visible = true;
    for i in coordinates.1 + 1..dimensions.1 {
        let coords = &(coordinates.0, i);
        let current = forrest.get(coords).unwrap();

        if current >= tree {
            is_visible = false;
            break;
        }
    }

    if is_visible {
        return 1;
    }

    0
}

fn calculate_scenic_score(
    forrest: &HashMap<(usize, usize), i32>,
    tree: &i32,
    coordinates: (usize, usize),
    dimensions: (usize, usize),
) -> i32 {
    if is_on_edge(coordinates, dimensions) {
        return 0;
    }

    let mut up = 0;
    for i in (0..coordinates.0).rev() {
        up += 1;
        let coords = &(i, coordinates.1);
        let current = forrest.get(coords).unwrap();

        if current >= tree {
            break;
        }
    }

    let mut down = 0;
    for i in coordinates.0 + 1..dimensions.0 {
        down += 1;
        let coords = &(i, coordinates.1);
        let current = forrest.get(coords).unwrap();

        if current >= tree {
            break;
        }
    }

    let mut left = 0;
    for i in (0..coordinates.1).rev() {
        left += 1;
        let coords = &(coordinates.0, i);
        let current = forrest.get(coords).unwrap();

        if current >= tree {
            break;
        }
    }

    let mut right = 0;
    for i in coordinates.1 + 1..dimensions.1 {
        right += 1;
        let coords = &(coordinates.0, i);
        let current = forrest.get(coords).unwrap();

        if current >= tree {
            break;
        }
    }

    let score = up * left * right * down;

    // println!(
    //     "Score for tree {:?} at coordinates {:?}: {:?}*{:?}*{:?}*{:?}={:?}",
    //     tree, coordinates, up, left, right, down, score
    // );

    score
}

fn is_on_edge(coordinates: (usize, usize), dimensions: (usize, usize)) -> bool {
    coordinates.0 == 0
        || coordinates.1 == 0
        || coordinates.0 == dimensions.0 - 1
        || coordinates.1 == dimensions.1 - 1
}

fn pt2(forrest: &HashMap<(usize, usize), i32>, dimensions: (usize, usize)) {
    let mut highest_scenic_score = -1;
    for i in 0..dimensions.0 {
        for j in 0..dimensions.1 {
            let tree = forrest.get(&(i, j)).unwrap();

            let score = calculate_scenic_score(forrest, tree, (i, j), dimensions);
            if score > highest_scenic_score {
                highest_scenic_score = score;
            }
        }
    }

    println!("Tree score {:?}", highest_scenic_score);
}

fn parse_input(input: Vec<&str>, forrest: &mut HashMap<(usize, usize), i32>) -> (usize, usize) {
    let mut n = 0;
    let mut m = 0;
    while n < input.len() {
        let line: Vec<char> = input[n].chars().collect();

        while m < line.len() {
            let tree = line[m].to_digit(10).unwrap() as i32;

            forrest.insert((n, m), tree);

            m += 1;
        }

        m = 0;
        n += 1;
    }

    (n, n)
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_this() {
        assert!(false);
    }
}
