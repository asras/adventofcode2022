use std::{collections::HashSet, fs};

pub fn main() {
    let input = fs::read_to_string("resources/06/input").expect("Kunne ikke åbne input");

    pt1(&input);
    pt2(&input);
}

fn pt1(buffer: &str) {
    let window_size = 4;
    let mut offset = 0;
    loop {
        let buffer_window = &buffer[offset..offset + window_size];

        let mut seen = HashSet::new();
        let mut repeat = false;
        for buffer_char in buffer_window.chars() {
            if seen.contains(&buffer_char) {
                repeat = true;
                break;
            }

            seen.insert(buffer_char);
        }

        if !repeat {
            break;
        }

        offset += 1;
    }

    println!("Length pt1 {:?}", offset + window_size);
}

fn pt2(buffer: &str) {
    let window_size = 14;
    let mut offset = 0;
    loop {
        let buffer_window = &buffer[offset..offset + window_size];

        let mut seen = HashSet::new();
        let mut repeat = false;
        for buffer_char in buffer_window.chars() {
            if seen.contains(&buffer_char) {
                repeat = true;
                break;
            }

            seen.insert(buffer_char);
        }

        if !repeat {
            break;
        }

        offset += 1;
    }

    println!("Length pt2 {:?}", offset + window_size);
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_this() {
        assert!(false);
    }
}
