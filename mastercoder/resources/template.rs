use std::fs;

pub fn main() {
    let input_str = fs::read_to_string("resources/TEMPLATE/input").expect("Kunne ikke åbne input");
    let input: Vec<&str> = input_str.split('\n').filter(|s| !s.is_empty()).collect();

    pt1();
    pt2();
}

fn pt1() {}

fn pt2() {}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_this() {
        assert!(false);
    }
}
