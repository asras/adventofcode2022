#!/bin/bash

day=$1

if [ -z "$day" ];
then
    day=$(date +'%d')
fi

if [ ! -d "resources/$day" ];
then
    mkdir "resources/$day"
fi

cp resources/template.rs "src/day${day}.rs"
sed -i '' "s/TEMPLATE/${day}/g" "src/day${day}.rs"
touch "resources/${day}/test" "resources/${day}/input"
